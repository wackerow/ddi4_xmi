call xalan -in DDI4_PIM_Lion.xmi -out DDI4_PIM_Lion_adjusted.xmi -xsl DDI4_PIM_Lion_adjust.xslt
echo.
echo ***** validate/transform DDI4_PIM_Lion_adjusted.xmi with local NIST XMI validator
echo ***** save to DDI4_PIM_NISTValidator.xmi
echo.
pause
rem set correct namespace (too complicated in XSLT)
sed --in-place --expression="s/2.4.1/20110701/g" DDI4_PIM_NISTValidator.xmi
del/q sed*
call xalan -in DDI4_PIM_NISTValidator.xmi -out DDI4_PIM_canonical.xmi -xsl DDI4_PIM_finalize.xslt