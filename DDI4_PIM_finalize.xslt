<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:mofext="http://www.omg.org/spec/MOF/20110701" exclude-result-prefixes="mofext">
	<!-- - - - - - - - - - - -->
	<xsl:output method="xml" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!-- - - - - - - - - - - -->
	<xsl:param name="DDI4_XMI_URI_Root" select=" 'http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PIM.xmi' "/>
	<xsl:param name="CommentsFilename" select=" 'CanonicalXMI_comments.xml' "/>
	<xsl:variable name="CommentsRoot" select="document( $CommentsFilename )"/>
	<!-- - - - - - - - - - - -->
	<!-- identity transform -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- insert comments -->
	<xsl:template match="/">
		<xsl:copy>
			<xsl:comment>
				<xsl:value-of select="$CommentsRoot"/>
			</xsl:comment>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- sort by package container name -->
	<xsl:template match="uml:Model">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="name"/>
			<xsl:apply-templates select="packagedElement[ @xmi:type='uml:Package' ]">
				<xsl:sort select="name"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- set UUID to structured URI -->
	<xsl:template match="@xmi:uuid[ name(..) ]">
		<xsl:variable name="DDI4_XMI_URI">
			<xsl:value-of select="$DDI4_XMI_URI_Root"/>
			<xsl:if test=" name(..)!='uml:Model' ">
				<xsl:text>#</xsl:text>
				<xsl:value-of select="../@xmi:id"/>
			</xsl:if>
		</xsl:variable>
		<xsl:attribute name="xmi:uuid"><xsl:value-of select="$DDI4_XMI_URI"/></xsl:attribute>
	</xsl:template>
	<!-- - - - - - - - - - - -->
</xsl:stylesheet>
