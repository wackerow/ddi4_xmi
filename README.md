# Canonical XMI Representation of DDI 4
## The Portable DDI UML Class Library

### Download of the Canonical XMI File
[`DDI4_PIM_canonical.xmi`](https://bitbucket.org/wackerow/ddi4_xmi/raw/master/DDI4_PIM_canonical.xmi) is the latest Canonical XMI representation of the DDI 4 UML Model.

## Production of the Canonical XMI File

1. Download `http://lion.ddialliance.org/xmi.xml` to `DDI4_PIM_Lion.xmi`
2. Adjust XMI format of `DDI4_PIM_Lion.xmi` with `DDI4_PIM_Lion_adjust.xslt`, result in `DDI4_PIM_Lion_adjusted.xmi`
3. Validate and transform to Canonical XMI with [NIST XMI Validator](http://validator.omg.org/se-interop/tools/validator).
   The [local version](http://validator.omg.org/se-interop/get-validator) seems to work a little better regarding the namespaces.
   Save result to `DDI4_PIM_NISTValidator.xmi`.
4. Finalize XMI format of `DDI4_PIM_NISTValidator.xmi` with `DDI4_PIM_finalize.xslt`, result in `DDI4_PIM_canonical.xmi`
   (adding appropriate UUIDs and making minor changes).

`download.cmd` has the command for step 1. `download_timestamp.txt` will be created for documenting the download time.

`run.cmd` has the commands for step 2 and 4; it pauses after step 2. Step 3 needs manual interaction.

For a description of Canonical XMI, see [annex B of the XML Metadata Interchange (XMI) Specification Version 2.5.1](https://www.omg.org/spec/XMI/2.5.1/PDF).

## Import into UML Tools

Successful imports of `DDI4_PIM_canonical.xmi` into the UML tools listed below. Some tools-specific files are available in the folder `bin`.

Please notice that only some tools are able to export a kind of portable XMI. This means that one is locked into a specific tool
if any changes are made to the model inside the tool.


### Open Source

- [Papyrus for UML](https://www.eclipse.org/papyrus/) Version 3.3.0 in Eclipse Modeling Tools Oxygen.3 Release (4.7.3)
- [Umbrello UML Modeller](https://umbrello.kde.org/) Version 2.24.3
- [UML Designer](http://www.umldesigner.org/) Version 8.0.0 (Eclipse-based)

### Commercial

- [Astah Professional](http://astah.net/editions) Version 7.2.0
- [Enterprise Architect](http://sparxsystems.com/products/ea/) Version 13.5 and 14.0, [free viewer](http://www.sparxsystems.eu/enterprisearchitect/ea-lite-edition/), DDI4_PIM.eapx (14.0)
- [IBM Rational Rhapsody](https://www.ibm.com/us-en/marketplace/uml-tools) Version 8.3
- [MagicDraw](https://www.nomagic.com/products/magicdraw) Version 18.5 and 19.0, [free viewer](https://www.magicdraw.com/download/reader) available, DDI4_PIM.mdzip
- [UModel](https://www.altova.com/umodel) Enterprise Edition Version 2018 rel. 2 sp1, DDI4_PIM.ump
- [Visual Paradigm](https://www.visual-paradigm.com/) Version 15.0

The folder `analyze-tools` contains some XSLTs for analyzing `DDI4_PIM_Lion.xmi`.

See also the information on the ongoing [development work on DDI 4](https://ddi-alliance.atlassian.net/wiki/spaces/DDI4/pages/491703/Moving+Forward+Project+DDI4)
and on [DDI in general and on the DDI Alliance](http://www.ddialliance.org/).