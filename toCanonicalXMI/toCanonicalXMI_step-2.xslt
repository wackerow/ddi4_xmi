<?xml version="1.0" encoding="UTF-8"?>
<!--
Transformation to Canonical XMI according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (here short CX)
	https://www.omg.org/spec/XMI/2.5.1/PDF
	from XMI export flavor of Enterprise Architect 14.1, exporter version 6.5
		Publish / Other Formats
			XML Type: UML 2.4.1 (XMI 2.4.2)
			"Export Diagrams" not checked
			"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XML.
CONFIGURE / Resources / Stylesheets (right-click)

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-2:
  Add EA documentation.
  Process only elements and properties from defined UML subset. Ignore other items
  Transform XML attributes to XML elements except xmi:id, xmi:uui, and xmi:type according CX B.2 5.
  Remove items if they have default values.
  Set correct URIs for UML primitives.
  Order elements according to CX.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:uml="http://www.omg.org/spec/UML/20131001" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org" xmlns:StandardProfile="http://www.omg.org/spec/UML/20131001/StandardProfile" extension-element-prefixes="exslt msxsl xalan">
	<!--
Set explicit encoding of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
Set DDI4_XMI_URI_Root -->
	<xsl:param name="DDI4_XMI_URI_Root" select=" 'http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PIM.xmi' "/>
	<!--
Set PrimitiveTypes URI-->
	<xsl:param name="PrimitiveTypesURI" select=" 'http://www.omg.org/spec/UML/20131001/PrimitiveTypes.xmi' "/>
	<!--
Set tool name -->
	<xsl:param name="Tool">
		<xsl:choose>
			<xsl:when test="/*[ local-name()='XMI']/*[ local-name()='Documentation']/@exporter = 'Enterprise Architect' ">
				<xsl:text>EnterpriseArchitect</xsl:text>
			</xsl:when>
			<!--
			<xsl:when test=" /xmi:XMI/uml:Model[2]/@name = 'RhapsodyStandardModel' ">
				<xsl:text>IBMRhapsody</xsl:text>
			</xsl:when>
			<xsl:when test=" /xmi:XMI/xmi:documentation/xmi:exporter = 'MagicDraw Clean XMI Exporter' ">
				<xsl:text>MagicDraw</xsl:text>
			</xsl:when>
			<xsl:when test="contains( /xmi:XMI/xmi:Documentation/exporter, 'Altova' )">
				<xsl:text>Umodel</xsl:text>
			</xsl:when>
			<xsl:when test="/xmi:XMI/uml:Model/xmi:Documentation/@exporter = 'Visual Paradigm' ">
				<xsl:text>VisualParadigm</xsl:text>
			</xsl:when>
-->
		</xsl:choose>
	</xsl:param>
	<!--
for translate -->
	<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
	<!--
Key for all IDs -->
	<xsl:key name="id" match="//*[@xmi:id]" use="@xmi:id"/>
	<!--
Key for all abstraction stereotypes -->
	<xsl:key name="base_Abstraction" match="//*" use="base_Abstraction/@xmi:idref"/>
	<!--
EA: Key for documentation in extension
-->
	<xsl:key name="EADoc" match="/xmi:XMI/xmi:Extension/connectors/connector | /xmi:XMI/xmi:Extension/elements/element | /xmi:XMI/xmi:Extension/elements/element/attributes/attribute" use="@xmi:idref"/>
	<!--
Entry point -->
	<xsl:template match="/">
		<xsl:apply-templates mode="step-2" select="xmi:XMI"/>
	</xsl:template>
	<!--
Process XMI element
-->
	<xsl:template mode="step-2" match="xmi:XMI">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="uml:Model"/>
			<xsl:apply-templates mode="step-2" select="StandardProfile:*">
				<xsl:sort select="local-name()"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Process StandardProfile elements
-->
	<xsl:template mode="step-2" match="StandardProfile:*">
		<xsl:copy-of select="."/>
	</xsl:template>
	<!--
Process Model
-->
	<xsl:template mode="step-2" match="uml:Model">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:call-template name="PackagedElementTypes"/>
		</xsl:copy>
	</xsl:template>
	<!--
Add documentation
-->
	<xsl:template name="Comment">
		<xsl:choose>
			<!-- if Enterprise Architect: add documentation from extension -->
			<xsl:when test=" $Tool = 'EnterpriseArchitect' ">
				<xsl:call-template name="EADocumentation">
					<xsl:with-param name="Documentation" select="key( 'EADoc', @xmi:id )/documentation/@value | key( 'EADoc', @xmi:id )/properties/@documentation"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates mode="step-2" select="ownedComment"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--
Enterprise Architect: documentation
-->
	<xsl:template name="EADocumentation">
		<xsl:param name="Documentation"/>
		<xsl:if test="$Documentation">
			<ownedComment xmi:id="dummy" xmi:type="uml:Comment">
				<annotatedElement xmi:idref="{@xmi:id}"/>
				<body>
					<xsl:value-of select="$Documentation"/>
				</body>
			</ownedComment>
		</xsl:if>
	</xsl:template>
	<!--
Process packagedElement uml:Abstraction
-->
	<xsl:template mode="step-2" match="packagedElement[ @xmi:type='uml:Abstraction' ]">
		<xsl:variable name="SupplierID" select="@supplier | supplier/@xmi:idref"/>
		<xsl:variable name="ClientID" select="@client | client/@xmi:idref"/>
		<xsl:variable name="Stereotype" select="translate( local-name( key('base_Abstraction', @xmi:id) ), $uppercase, $lowercase)"/>
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:choose>
				<xsl:when test="name">
					<xsl:apply-templates mode="step-2" select="name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="name">
						<xsl:choose>
							<xsl:when test="$Stereotype = 'derive' ">
								<xsl:text>derivesFrom</xsl:text>
							</xsl:when>
							<xsl:when test="$Stereotype = 'refine' ">
								<xsl:text>refines</xsl:text>
							</xsl:when>
							<xsl:when test="$Stereotype = 'trace' ">
								<xsl:text>tracesTo</xsl:text>
							</xsl:when>
						</xsl:choose>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:element name="client">
				<xsl:attribute name="xmi:idref">
					<xsl:value-of select="$ClientID"/>
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="supplier">
				<xsl:attribute name="xmi:idref">
					<xsl:value-of select="$SupplierID"/>
				</xsl:attribute>
			</xsl:element>
		</xsl:copy>
	</xsl:template>
	<!--
Process packagedElement uml:Association
-->
	<xsl:template mode="step-2" match="packagedElement[ @xmi:type='uml:Association' ]">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:apply-templates mode="step-2" select="@memberEnd | memberEnd"/>
			<xsl:apply-templates mode="step-2" select="ownedEnd"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process lowerValue in ownedAttribute and ownedEnd
-->
	<xsl:template mode="step-2" match="lowerValue">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@value | value"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process lowerValue in ownedAttribute and ownedEnd
-->
	<xsl:template mode="step-2" match="upperValue">
		<xsl:variable name="Value" select="@value | value"/>
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@value | value"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process value
-->
	<xsl:template mode="step-2" match="@value | value">
		<!-- nothing set if 0, 0 is default for LiteralInteger -->
		<xsl:if test=" . != '0' ">
			<xsl:element name="value">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process packagedElement uml:Class
-->
	<xsl:template mode="step-2" match="packagedElement[ @xmi:type='uml:Class' ]">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:apply-templates mode="step-2" select="generalization"/>
			<xsl:apply-templates mode="step-2" select="@isAbstract | isAbstract"/>
			<xsl:apply-templates mode="step-2" select="ownedAttribute"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process generalization in packagedElement uml:Class
-->
	<xsl:template mode="step-2" match="generalization">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@general | general"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process general in generalization
-->
	<xsl:template mode="step-2" match="@general">
		<xsl:element name="general">
			<xsl:attribute name="xmi:idref">
				<xsl:value-of select="."/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="general">
		<xsl:copy>
			<xsl:attribute name="xmi:idref">
				<xsl:value-of select="@xmi:idref | @href"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<!--
Process isAbstract in packagedElement uml:Class
-->
	<xsl:template mode="step-2" match="@isAbstract | isAbstract">
		<xsl:if test=" . = 'true' ">
			<xsl:element name="isAbstract">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process ownedAttribute in packagedElement uml:Class and uml:DataType
-->
	<xsl:template mode="step-2" match="ownedAttribute">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<!--			<xsl:choose>
				-->
			<!-- if Enterprise Architect: add documentation from extension -->
			<!--
				<xsl:when test=" $Tool = 'EnterpriseArchitect' ">
					<xsl:call-template name="EADocumentation">
						<xsl:with-param name="Documentation" select="key( 'EADoc', @xmi:id )/documentation/@value"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="step-2" select="ownedComment"/>
				</xsl:otherwise>
			</xsl:choose>
-->
			<xsl:apply-templates mode="step-2" select="lowerValue"/>
			<xsl:apply-templates mode="step-2" select="upperValue"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:apply-templates mode="step-2" select="@aggregation | aggregation"/>
			<xsl:apply-templates mode="step-2" select="@association | association"/>
			<xsl:apply-templates mode="step-2" select="defaultValue"/>
			<xsl:apply-templates mode="step-2" select="@isID | isID"/>
			<xsl:if test=" $Tool = 'EnterpriseArchitect' ">
				<xsl:call-template name="EAisID"/>
			</xsl:if>
			<xsl:apply-templates mode="step-2" select="@isOrdered | isOrdered"/>
			<xsl:apply-templates mode="step-2" select="@isReadOnly | isReadOnly"/>
			<xsl:apply-templates mode="step-2" select="@type | type"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process association in ownedAttribute and ownedEnd
-->
	<xsl:template mode="step-2" match="@association">
		<xsl:element name="association">
			<xsl:attribute name="xmi:idref">
				<xsl:value-of select="."/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="association">
		<xsl:element name="association">
			<xsl:attribute name="xmi:idref">
				<xsl:value-of select="@xmi:idref | @href"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--
Process defaultValue in ownedAttribute
-->
	<xsl:template mode="step-2" match="defaultValue">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<value>
				<xsl:value-of select="@value | value"/>
			</value>
		</xsl:copy>
	</xsl:template>
	<!--
Process isID in ownedAttribute
-->
	<xsl:template mode="step-2" match="@isID | isID">
		<xsl:if test=" . = 'true' ">
			<xsl:element name="isID">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Enterprise Architect isID
-->
	<xsl:template name="EAisID">
		<xsl:if test=" contains( key( 'EADoc', @xmi:id )/xrefs/@value, '$DES=@PROP=@NAME=isID@ENDNAME;' )">
			<xsl:element name="isID">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process isOrdered in ownedAttribute
-->
	<xsl:template mode="step-2" match="@isOrdered | isOrdered">
		<xsl:if test=" . = 'true' ">
			<xsl:element name="isOrdered">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process packagedElement uml:DataType
-->
	<xsl:template mode="step-2" match="packagedElement[ @xmi:type='uml:DataType' ]">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:apply-templates mode="step-2" select="generalization"/>
			<xsl:apply-templates mode="step-2" select="ownedAttribute"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process packagedElement uml:Enumeration
-->
	<xsl:template mode="step-2" match="packagedElement[ @xmi:type='uml:Enumeration' ]">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:apply-templates mode="step-2" select="generalization"/>
			<xsl:apply-templates mode="step-2" select="ownedLiteral"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process packagedElement uml:Package
-->
	<xsl:template mode="step-2" match="packagedElement[ @xmi:type='uml:Package' ]">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:call-template name="PackagedElementTypes"/>
		</xsl:copy>
	</xsl:template>
	<!--
Package types
-->
	<xsl:template name="PackagedElementTypes">
		<xsl:apply-templates mode="step-2" select="packagedElement[ @xmi:type='uml:Abstraction' ]"/>
		<xsl:apply-templates mode="step-2" select="packagedElement[ @xmi:type='uml:Association' ]"/>
		<xsl:apply-templates mode="step-2" select="packagedElement[ @xmi:type='uml:Class' ]"/>
		<xsl:apply-templates mode="step-2" select="packagedElement[ @xmi:type='uml:DataType' ]"/>
		<xsl:apply-templates mode="step-2" select="packagedElement[ @xmi:type='uml:Enumeration' ]"/>
		<xsl:apply-templates mode="step-2" select="packagedElement[ @xmi:type='uml:Package' ]"/>
		<xsl:apply-templates mode="step-2" select="packagedElement[ @xmi:type='uml:PrimitiveType' ]"/>
	</xsl:template>
	<!--
Process packagedElement uml:PrimitiveType
-->
	<xsl:template mode="step-2" match="packagedElement[ @xmi:type='uml:PrimitiveType' ]">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
			<xsl:apply-templates mode="step-2" select="generalization"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process name in ownedAttribute and packagedElement
-->
	<xsl:template mode="step-2" match="@name | name">
		<xsl:element name="name">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>
	<!--
Process memberEnd in packagedElement uml:Association
-->
	<xsl:template mode="step-2" match="@memberEnd">
		<!-- if two references like in Eclipse -->
		<xsl:choose>
			<xsl:when test="contains( ., ' ' )">
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref">
						<xsl:value-of select="substring-before( ., ' ' )"/>
					</xsl:attribute>
				</xsl:element>
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref">
						<xsl:value-of select="substring-after( ., ' ' )"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref">
						<xsl:value-of select="."/>
					</xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="step-2" match="memberEnd">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:idref"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process ownedEnd in packagedElement uml:Association
-->
	<xsl:template mode="step-2" match="ownedEnd">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:apply-templates select="lowerValue" mode="step-2"/>
			<xsl:apply-templates select="upperValue" mode="step-2"/>
			<xsl:apply-templates select="@name | name" mode="step-2"/>
			<xsl:apply-templates select="@aggregation | aggregation" mode="step-2"/>
			<xsl:apply-templates select="@association | association" mode="step-2"/>
			<xsl:apply-templates select="@type | type" mode="step-2"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process type in ownedAttribute and ownedEnd
-->
	<xsl:template mode="step-2" match="@type">
		<xsl:call-template name="type">
			<xsl:with-param name="Value" select="."/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="step-2" match="type">
		<xsl:variable name="OldReferenceValue">
			<xsl:choose>
				<xsl:when test="@href">
					<xsl:value-of select="@href"/>
				</xsl:when>
				<xsl:when test="@xmi:idref">
					<xsl:value-of select="@xmi:idref"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="OldReferenceValue2">
			<!-- deal with EA inconsistency -->
			<xsl:choose>
				<xsl:when test="$OldReferenceValue = 'EAJava_int' ">
					<xsl:text>EAJava_Integer</xsl:text>
				</xsl:when>
				<xsl:when test="$OldReferenceValue = 'EAJava_boolean' ">
					<xsl:text>EAJava_Boolean</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$OldReferenceValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="type">
			<xsl:with-param name="OldReferenceValue" select="$OldReferenceValue2"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="type">
		<xsl:param name="OldReferenceValue"/>
		<xsl:variable name="NewReferenceValue">
			<!-- Determine appropriate reference. The tools have a large variety regarding the UML primitives -->
			<xsl:choose>
				<!-- if any external reference to a primitive like with canonical, eclipse, IBM Rhapsody, MagicDraw, Modelio -->
				<xsl:when test="contains( $OldReferenceValue, '//' ) and contains( $OldReferenceValue, '#' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="substring-after( $OldReferenceValue, '#' )"/>
				</xsl:when>
				<!-- if internal references to primitives of Astah -->
				<xsl:when test="contains( key( 'id', $OldReferenceValue )/../../@name, 'Primitive Package' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $OldReferenceValue )/../@name"/>
				</xsl:when>
				<!-- if internal references to primitives of Enterprise Architect  -->
				<xsl:when test=" $OldReferenceValue = 'EAJava_Boolean' or  $OldReferenceValue = 'EAJava_Integer' or  $OldReferenceValue = 'EAJava_Real' or  $OldReferenceValue = 'EAJava_String' or  $OldReferenceValue = 'EAJava_UnlimitedNatural' ">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="substring-after($OldReferenceValue, 'EAJava_')"/>
					<!--
					<xsl:value-of select="key( 'id', $OldReferenceValue )/@name"/>
					-->
				</xsl:when>
				<!-- if internal references to primitives of UModel -->
				<xsl:when test="contains( key( 'id', $OldReferenceValue )/../../@name, 'PrimitiveTypes' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $OldReferenceValue )/../@name"/>
				</xsl:when>
				<!-- if internal references to primitives of Visual Paradigm -->
				<xsl:when test=" $OldReferenceValue = 'boolean_id' or  $OldReferenceValue = 'int_id' or  $OldReferenceValue = 'Real_id' or  $OldReferenceValue = 'string_id' or  $OldReferenceValue = 'UnlimitedNatural_id' ">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:variable name="Name" select="key( 'id', $OldReferenceValue )/../@name"/>
					<xsl:choose>
						<xsl:when test=" $Name = 'boolean' ">
							<xsl:text>Boolean</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'int' ">
							<xsl:text>Integer</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'float' ">
							<xsl:text>Real</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'string' ">
							<xsl:text>String</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'UnlimitedNatural' ">
							<xsl:text>UnlimitedNatural</xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
				<!-- regular internal reference -->
				<xsl:otherwise>
					<xsl:value-of select="$OldReferenceValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:element name="type">
			<xsl:choose>
				<xsl:when test="starts-with( $NewReferenceValue, 'http://' )">
					<xsl:attribute name="xmi:type">
						<xsl:text>uml:PrimitiveType</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="href">
						<xsl:value-of select="$NewReferenceValue"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="xmi:idref">
						<xsl:value-of select="$NewReferenceValue"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<!--
Process ownedComment
-->
	<xsl:template mode="step-2" match="ownedComment">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@annotatedElement | annotatedElement"/>
			<xsl:apply-templates mode="step-2" select="@body | body"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process aggregation in ownedAttribute and ownedEnd 
-->
	<xsl:template mode="step-2" match="@aggregation | aggregation">
		<!-- if not default -->
		<xsl:if test=" . != 'none' ">
			<xsl:element name="aggregation">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process isReadOnly in ownedAttribute 
-->
	<xsl:template mode="step-2" match="@isReadOnly">
		<!-- if not association and not default -->
		<xsl:if test=" not(../@association) and . != 'false' ">
			<xsl:element name="isReadOnly">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="step-2" match="isReadOnly">
		<!-- if not association and not default -->
		<xsl:if test=" not(../association) and . != 'false' ">
			<xsl:element name="isReadOnly">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process body in ownedComment
-->
	<xsl:template mode="step-2" match="@body | body">
		<xsl:element name="body">
			<xsl:value-of select="."/>
		</xsl:element>
		<!-- set reference to parent packagedElement if annotation reference does not exist -->
		<xsl:if test="not( ../@annotatedElement ) and not( ../annotatedElement )">
			<xsl:element name="annotatedElement">
				<xsl:attribute name="xmi:idref">
					<xsl:value-of select="parent::ownedComment/parent::*/@xmi:id"/>
				</xsl:attribute>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process annotatedElement in ownedComment
-->
	<xsl:template mode="step-2" match="@annotatedElement">
		<xsl:element name="annotatedElement">
			<xsl:attribute name="xmi:idref">
				<xsl:value-of select="."/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="annotatedElement">
		<xsl:element name="annotatedElement">
			<xsl:attribute name="xmi:idref">
				<xsl:value-of select="@xmi:idref | @href"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--
Process ownedLiteral in packagedElement uml:EnumerationLiteral
-->
	<xsl:template mode="step-2" match="ownedLiteral">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:apply-templates mode="step-2" select="@xmi:type"/>
			<xsl:call-template name="Comment"/>
			<xsl:apply-templates mode="step-2" select="@name | name"/>
		</xsl:copy>
	</xsl:template>
	<!--
copy attributes
-->
	<xsl:template mode="step-2" match="@*">
		<xsl:copy/>
	</xsl:template>
</xsl:stylesheet>