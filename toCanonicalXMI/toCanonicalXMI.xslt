<?xml version="1.0" encoding="UTF-8"?>
<!--
Transformation from proprietary XMI flavors to Canonical XMI according to
"Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (short CX)
https://www.omg.org/spec/XMI/2.5.1/PDF

The transformation expects a limited subset of UML/XMI class diagram items.

Covered UML Items:

Structural Items
  Model
  Package
  Class
  Property

Relationships
  Association
  Generalization

Data types
  DataType
  EnumerationLiteral
  Enumeration
  LiteralInteger
  LiteralString
  LiteralUnlimitedNatural
  PrimitiveType

Other
  Comment

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Enterprise Architect version 14 and 15

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XMI.
CONFIGURE / Resources / Stylesheets (right-click)

Export of XMI
	Publish / Other Formats
		XML Type: UML 2.4.1 (XMI 2.4.2)
		"Export Diagrams" not checked
		"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Processing takes place in four steps.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" extension-element-prefixes="exslt msxsl">
	<!--
Set explicit encoding of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
Set DDI4_XMI_URI_Root -->
	<xsl:param name="DDI4_XMI_URI_Root" select=" 'http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PIM.xmi' "/>
	<!--
Set PrimitiveTypes URI-->
	<xsl:param name="PrimitiveTypesURI" select=" 'http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi' "/>
	<!--
EA: Key for documentation in extension
-->
	<xsl:key name="EADoc" match="/xmi:XMI/xmi:Extension/elements/element | /xmi:XMI/xmi:Extension/elements/element/attributes/attribute" use="@xmi:idref"/>
	<!--
Key for all IDs -->
	<xsl:key name="id" match="//*[@xmi:id]" use="@xmi:id"/>
	<!--
Set tool name -->
	<xsl:param name="Tool">
		<xsl:choose>
			<xsl:when test="/*[ local-name()='XMI']/*[ local-name()='Documentation']/@exporter = 'Enterprise Architect' ">
				<xsl:text>EnterpriseArchitect</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:param>
	<!--
Entry point and 4 process steps. -->
	<xsl:template match="/">
		<xsl:variable name="Result-1">
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='XMI' or local-name()='Model' ]"/>
		</xsl:variable>
		<xsl:variable name="Result-2">
			<!-- todo -->
			<xsl:choose>
				<xsl:when test="function-available('exslt:node-set')">
					<xsl:apply-templates mode="step-2" select="exslt:node-set( $Result-1 )/xmi:XMI"/>
				</xsl:when>
				<xsl:when test="function-available('msxsl:node-set')">
					<xsl:apply-templates mode="step-2" select="msxsl:node-set( $Result-1 )/xmi:XMI"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="Result-3">
			<xsl:choose>
				<xsl:when test="function-available('exslt:node-set')">
					<xsl:apply-templates mode="step-3" select="exslt:node-set( $Result-2 )/node()"/>
				</xsl:when>
				<xsl:when test="function-available('msxsl:node-set')">
					<xsl:apply-templates mode="step-3" select="msxsl:node-set( $Result-2 )/node()"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="function-available('exslt:node-set')">
				<xsl:apply-templates mode="step-4" select="exslt:node-set( $Result-3 )/node()"/>
			</xsl:when>
			<xsl:when test="function-available('msxsl:node-set')">
				<xsl:apply-templates mode="step-4" select="msxsl:node-set( $Result-3 )/node()"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step 1: 
  Ignoring namespaces, setting namespaces to UML 2.4.1 / XMI 2.4.1. This version is recommended because it is implemented in many UML tools.
  Add XMI root if it doesn't exist.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

process element xmi:XMI -->
	<xsl:template mode="step-1" match="node()[ local-name()='XMI' ]">
		<xmi:XMI xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:xmi="http://www.omg.org/spec/XMI/20110701">
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='Documentation' or local-name()='documentation' ]"/>
			<!-- process only first element uml:Model like in case of IBM Rhapsody-->
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='Model' ][1]"/>
			<!-- process element xmi:Extension important in case of Enterprise Architect -->
			<xsl:apply-templates mode="step-1" select="node()[ local-name()='Extension' ]"/>
		</xmi:XMI>
	</xsl:template>
	<!--
process element uml:Model -->
	<xsl:template mode="step-1" match="node()[ local-name()='Model' ]">
		<xsl:choose>
			<!-- if no XMI element like in case of Modelio -->
			<xsl:when test="count(ancestor::*) = 0">
				<xmi:XMI xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:xmi="http://www.omg.org/spec/XMI/20110701">
					<uml:Model>
						<xsl:apply-templates mode="step-1" select="@* | node()"/>
						<xsl:if test="not( @*[ local-name() = 'id' ] )">
							<xsl:attribute name="xmi:id"><xsl:text>dummy</xsl:text></xsl:attribute>
						</xsl:if>
					</uml:Model>
				</xmi:XMI>
			</xsl:when>
			<xsl:when test="@name = 'EA_Model' ">
				<xsl:variable name="ModelName" select="packagedElement/@name"/>
				<uml:Model xmi:id="dummy">
					<xsl:attribute name="name"><xsl:value-of select="packagedElement/@name"></xsl:value-of></xsl:attribute>
					<!-- skip the packagedElement containing the model name -->
					<!--
					<xsl:apply-templates mode="step-1" select="packagedElement/*"/>
-->
					<xsl:apply-templates mode="step-1" select="node()"/>
				</uml:Model>
			</xsl:when>
			<xsl:otherwise>
				<uml:Model>
					<xsl:if test="not( @*[ local-name() = 'id' ] )">
						<xsl:attribute name="xmi:id"><xsl:text>dummy</xsl:text></xsl:attribute>
					</xsl:if>
					<xsl:apply-templates mode="step-1" select="@* | node()"/>
				</uml:Model>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--
process element xmi:Extension -->
	<xsl:template mode="step-1" match="node()[ local-name()='Documentation' or local-name()='Extension' ]">
		<xsl:variable name="QualifiedName" select="concat( 'xmi:', local-name() )"/>
		<xsl:element name="{$QualifiedName}">
			<xsl:apply-templates mode="step-1" select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	<!--
copy other elements -->
	<xsl:template mode="step-1" match="node()">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates mode="step-1" select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	<!--
skip attributes 
	<xsl:template mode="step-1" match="@*[ local-name()='version' or local-name()='Profile' ]"/>
-->
	<!--
copy attributes with prefix xmi -->
	<xsl:template mode="step-1" match="@*[ local-name()='id' or local-name()='uuid' or local-name()='type' or local-name()='idref' ]">
		<xsl:variable name="FullName" select="concat( 'xmi:', local-name() )"/>
		<xsl:attribute name="{$FullName}"><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>
	<!--
copy other attributes -->
	<xsl:template mode="step-1" match="@*">
		<xsl:attribute name="{local-name()}"><xsl:value-of select="."/></xsl:attribute>
	</xsl:template>
	<!--
copy the rest of the nodes -->
	<xsl:template mode="step-1" match="comment() | text() | processing-instruction()">
		<xsl:copy/>
	</xsl:template>
	<!--
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-2:
  Add EA documentation.
  Process only elements and properties from defined UML subset.
  Transform XML attributes to XML elements except xmi:id, xmi:uui, and xmi:type according CX B.2 5.
  Remove values if they are default values.
  Set correct URIs for UML primitives

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Process XMI element -->
	<xsl:template mode="step-2" match="xmi:XMI">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="uml:Model"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process Model
-->
	<xsl:template mode="step-2" match="uml:Model">
		<xsl:variable name="Name" select="@name | name"/>
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@name | name | ownedComment | packagedElement[ @xmi:type='uml:Package' or @xmi:type='uml:Class' or @xmi:type='uml:Association' or @xmi:type='uml:DataType' or @xmi:type='uml:Enumeration' or @xmi:type='uml:PrimitiveType' ]">
				<xsl:with-param name="Name" select="$Name"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Process packagedElement
-->
	<xsl:template mode="step-2" match="packagedElement">
		<xsl:variable name="Name" select="@name | name"/>
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="generalization | @isAbstract | isAbstract | @name | name | @memberEnd | memberEnd | ownedAttribute | ownedComment | ownedEnd | packagedElement[ @xmi:type='uml:Package' or @xmi:type='uml:Class' or @xmi:type='uml:Association' or @xmi:type='uml:DataType' or @xmi:type='uml:Enumeration' or @xmi:type='uml:PrimitiveType' ] | ownedLiteral">
				<xsl:with-param name="Name" select="$Name"/>
			</xsl:apply-templates>
			<!-- if Enterprise Architect: add documentation from extension -->
			<xsl:if test=" $Tool = 'EnterpriseArchitect' ">
				<xsl:call-template name="EADocumentation">
					<xsl:with-param name="Documentation" select="key( 'EADoc', @xmi:id )/properties/@documentation"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--
Process generalization in packagedElement
-->
	<xsl:template mode="step-2" match="generalization">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@general | general | ownedComment"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process general in generalization
-->
	<xsl:template mode="step-2" match="@general">
		<xsl:element name="general">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="general">
		<xsl:copy>
			<xsl:attribute name="xmi:idref"><xsl:value-of select="@xmi:idref | @href"/></xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<!--
Process isAbstract in packagedElement
-->
	<xsl:template mode="step-2" match="@isAbstract | isAbstract">
		<xsl:if test=" . = 'true' ">
			<xsl:element name="isAbstract">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process ownedAttribute in packagedElement
-->
	<xsl:template mode="step-2" match="ownedAttribute">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@aggregation | @association | association | @isID | isID | @isReadOnly | isReadOnly | lowerValue | @name | name | ownedComment | @type | type | upperValue"/>
			<!-- if Enterprise Architect: add documentation from extension -->
			<xsl:if test=" $Tool = 'EnterpriseArchitect' ">
				<xsl:call-template name="EADocumentation">
					<xsl:with-param name="Documentation" select="key( 'EADoc', @xmi:id )/documentation/@value"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--
Process association in ownedAttribute and ownedEnd
-->
	<xsl:template mode="step-2" match="@association">
		<xsl:element name="association">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="association">
		<xsl:element name="association">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="@xmi:idref | @href"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--
Process isID in ownedAttribute
-->
	<xsl:template mode="step-2" match="@isID | isID">
		<xsl:if test=" . = 'true' ">
			<xsl:element name="isID">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process lowerValue in ownedAttribute
-->
	<xsl:template mode="step-2" match="lowerValue">
		<!-- nothing set if 1, is default for lowerValue -->
		<xsl:if test=" ( @value and @value != '1' ) or ( value and value != '1' ) or not( @value and value ) ">
			<xsl:copy>
				<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
				<xsl:apply-templates mode="step-2" select="@value | value"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--
Process upperValue in ownedAttribute
-->
	<xsl:template mode="step-2" match="upperValue">
		<xsl:variable name="Value" select="@value | value"/>
		<!-- nothing set if 1, is default for upperValue -->
		<!--
		<xsl:if test=" @value and @value != '1' or value and value != '1' ">
-->
		<xsl:element name="upperValue">
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:choose>
				<xsl:when test=" $Value = '*' ">
					<xsl:attribute name="xmi:type"><xsl:text>uml:LiteralUnlimitedNatural</xsl:text></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="xmi:type"><xsl:text>uml:LiteralInteger</xsl:text></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates mode="step-2" select="@value | value"/>
		</xsl:element>
		<!--
		</xsl:if>
-->
	</xsl:template>
	<!--
Process value in lowerValue
-->
	<xsl:template mode="step-2" match="@value | value">
		<!-- nothing set if 0, is default for value-->
		<xsl:if test="not( local-name(..) = 'lowerValue' and . = '0' )">
			<xsl:element name="value">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process name in packagedElement
-->
	<xsl:template mode="step-2" match="@name | name">
		<xsl:element name="name">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>
	<!--
Process memberEnd in packagedElement
-->
	<xsl:template mode="step-2" match="@memberEnd">
		<!-- if two references like in Eclipse -->
		<xsl:choose>
			<xsl:when test="contains( ., ' ' )">
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref"><xsl:value-of select="substring-before( ., ' ' )"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref"><xsl:value-of select="substring-after( ., ' ' )"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="step-2" match="memberEnd">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:idref"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process ownedEnd in packagedElement
-->
	<xsl:template mode="step-2" match="ownedEnd">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates select="@aggregation | @association | association | lowerValue | ownedComment | @type | type | upperValue" mode="step-2"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process type in ownedEnd
-->
	<xsl:template mode="step-2" match="@type">
		<xsl:call-template name="type">
			<xsl:with-param name="Value" select="."/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="step-2" match="type">
		<xsl:variable name="OldReferenceValue">
			<xsl:choose>
				<xsl:when test="@href">
					<xsl:value-of select="@href"/>
				</xsl:when>
				<xsl:when test="@xmi:idref">
					<xsl:value-of select="@xmi:idref"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="type">
			<xsl:with-param name="OldReferenceValue" select="$OldReferenceValue"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="type">
		<xsl:param name="OldReferenceValue"/>
		<xsl:variable name="NewReferenceValue">
			<!-- Determine appropriate reference. The tools have a large variety regarding the UML primitives -->
			<xsl:choose>
				<!-- if any external reference to a primitive like with canonical, eclipse, IBM Rhapsody, MagicDraw, Modelio -->
				<xsl:when test="contains( $OldReferenceValue, '//' ) and contains( $OldReferenceValue, '#' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="substring-after( $OldReferenceValue, '#' )"/>
				</xsl:when>
				<!-- if internal references to primitives of Astah -->
				<xsl:when test="contains( key( 'id', $OldReferenceValue )/../../@name, 'Primitive Package' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $OldReferenceValue )/../@name"/>
				</xsl:when>
				<!-- if internal references to primitives of Enterprise Architect  -->
				<xsl:when test=" $OldReferenceValue = 'EAJava_Boolean' or  $OldReferenceValue = 'EAJava_Integer' or  $OldReferenceValue = 'EAJava_Real' or  $OldReferenceValue = 'EAJava_String' or  $OldReferenceValue = 'EAJava_UnlimitedNatural' ">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $OldReferenceValue )/@name"/>
				</xsl:when>
				<!-- if internal references to primitives of UModel -->
				<xsl:when test="contains( key( 'id', $OldReferenceValue )/../../@name, 'PrimitiveTypes' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $OldReferenceValue )/../@name"/>
				</xsl:when>
				<!-- if internal references to primitives of Visual Paradigm -->
				<xsl:when test=" $OldReferenceValue = 'boolean_id' or  $OldReferenceValue = 'int_id' or  $OldReferenceValue = 'Real_id' or  $OldReferenceValue = 'string_id' or  $OldReferenceValue = 'UnlimitedNatural_id' ">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:variable name="Name" select="key( 'id', $OldReferenceValue )/../@name"/>
					<xsl:choose>
						<xsl:when test=" $Name = 'boolean' ">
							<xsl:text>Boolean</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'int' ">
							<xsl:text>Integer</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'Real' ">
							<xsl:text>Real</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'string' ">
							<xsl:text>String</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'UnlimitedNatural' ">
							<xsl:text>UnlimitedNatural</xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
				<!-- regular internal reference -->
				<xsl:otherwise>
					<xsl:value-of select="$OldReferenceValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:element name="type">
			<xsl:choose>
				<xsl:when test="starts-with( $NewReferenceValue, 'http://' )">
					<xsl:attribute name="href"><xsl:value-of select="$NewReferenceValue"/></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="xmi:idref"><xsl:value-of select="$NewReferenceValue"/></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<!--
Process ownedComment in packagedElement
-->
	<xsl:template mode="step-2" match="ownedComment">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@annotatedElement | annotatedElement | @body | body"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process aggregation in ownedAttribute and ownedEnd 
-->
	<xsl:template mode="step-2" match="@aggregation | aggregation">
		<!-- if not default -->
		<xsl:if test=" . != 'none' ">
			<xsl:element name="aggregation">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process aggregation in ownedAttribute and ownedEnd 
-->
	<xsl:template mode="step-2" match="@isReadOnly">
		<!-- if not association and not default -->
		<xsl:if test=" not(../@association) and . != 'false' ">
			<xsl:element name="isReadOnly">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="step-2" match="isReadOnly">
		<!-- if not association and not default -->
		<xsl:if test=" not(../association) and . != 'false' ">
			<xsl:element name="isReadOnly">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process body in ownedComment
-->
	<xsl:template mode="step-2" match="@body | body">
		<xsl:element name="body">
			<xsl:value-of select="."/>
		</xsl:element>
		<!-- set reference to parent packagedElement if annotation reference does not exist -->
		<xsl:if test="not( ../@annotatedElement ) and not( ../annotatedElement )">
			<xsl:element name="annotatedElement">
				<xsl:attribute name="xmi:idref"><xsl:value-of select="parent::ownedComment/parent::*/@xmi:id"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process annotatedElement in ownedComment
-->
	<xsl:template mode="step-2" match="@annotatedElement">
		<xsl:element name="annotatedElement">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="annotatedElement">
		<xsl:element name="annotatedElement">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="@xmi:idref | @href"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--
Process ownedLiteral in packagedElement
-->
	<xsl:template mode="step-2" match="ownedLiteral">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@annotatedElement | annotatedElement | @body | body | @name | name"/>
		</xsl:copy>
	</xsl:template>
	<!--
Enterprise Architect: documentation
-->
	<xsl:template name="EADocumentation">
		<xsl:param name="Documentation"/>
		<xsl:if test="$Documentation">
			<ownedComment xmi:id="dummy" xmi:type="uml:Comment">
				<annotatedElement xmi:idref="{@xmi:id}"/>
				<body>
					<xsl:value-of select="$Documentation"/>
				</body>
			</ownedComment>
		</xsl:if>
	</xsl:template>
	<!--
copy attributes
-->
	<xsl:template mode="step-2" match="@*">
		<xsl:copy/>
	</xsl:template>
	<!--
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-3:
  create xmi:id and xmi:uuid according to CX

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Process nodes -->
	<xsl:template mode="step-3" match="node()">
		<xsl:copy>
			<xsl:apply-templates mode="step-3" select="@*"/>
			<xsl:apply-templates mode="step-3" select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process attributes -->
	<xsl:template mode="step-3" match="@*">
		<xsl:copy/>
	</xsl:template>
	<!--
Process attribute id -->
	<xsl:template mode="step-3" match="@xmi:id">
		<xsl:variable name="IdValue">
			<xsl:call-template name="CreateID">
				<xsl:with-param name="Node" select="."/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="UuidValue" select="concat( $DDI4_XMI_URI_Root, '#', $IdValue)"/>
		<xsl:attribute name="xmi:id"><xsl:value-of select="$IdValue"></xsl:value-of></xsl:attribute>
		<xsl:attribute name="xmi:uuid"><xsl:value-of select="$UuidValue"></xsl:value-of></xsl:attribute>
	</xsl:template>
	<!--
Process attribute idref -->
	<xsl:template mode="step-3" match="@xmi:idref">
		<xsl:variable name="IdValue">
			<xsl:call-template name="CreateID">
				<xsl:with-param name="Node" select="key( 'id', . )"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:attribute name="xmi:idref"><xsl:value-of select="$IdValue"></xsl:value-of></xsl:attribute>
		<xsl:if test="$IdValue = '' ">
			<xsl:message>
				<xsl:value-of select="."/>
			</xsl:message>
		</xsl:if>
	</xsl:template>
	<!--
Create id according CX -->
	<xsl:template name="CreateID">
		<xsl:param name="Node"/>
		<!-- set node environment -->
		<xsl:for-each select="$Node">
			<!-- without root node -->
			<xsl:for-each select="ancestor-or-self::*[parent::*]">
				<xsl:variable name="Name" select="name"/>
				<xsl:choose>
					<xsl:when test="name">
						<xsl:value-of select="name"/>
						<!-- deal with associations which have non-unique names in the same package -->
						<xsl:if test=" @xmi:type='uml:Association' and count( following-sibling::packagedElement[ @xmi:type='uml:Association' ][ name=$Name ] ) > 0 ">
							<xsl:variable name="SequenceNumber" select="count( preceding-sibling::packagedElement[ @xmi:type='uml:Association' ][ name=$Name ] ) + 1"/>
							<!-- add sequence number -->
							<xsl:text>-</xsl:text>
							<xsl:value-of select="$SequenceNumber"/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="local-name()"/>
						<!-- deal with ownedAttribute without names -->
						<xsl:if test=" local-name()='ownedAttribute' ">
							<xsl:variable name="SequenceNumber" select="count( preceding-sibling::ownedAttribute ) + 1"/>
							<!-- add sequence number -->
							<xsl:text>-</xsl:text>
							<xsl:value-of select="$SequenceNumber"/>
						</xsl:if>
						<!-- deal with association without names -->
						<xsl:if test=" local-name()='packagedElement' and @xmi:type='uml:Association' ">
							<xsl:variable name="SequenceNumber" select="count( preceding-sibling::packagedElement[ @xmi:type='uml:Association' ] ) + 1"/>
							<!-- add sequence number -->
							<xsl:text>-</xsl:text>
							<xsl:value-of select="$SequenceNumber"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="not( position()=last() )">
					<xsl:text>-</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
	<!--
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-4:
  Sort elements according to CX

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Process nodes and sort -->
	<xsl:template mode="step-4" match="node()[ @xmi:type='uml:Class' or @xmi:type='uml:Property' ]">
		<xsl:copy>
			<xsl:call-template name="AttributeOrder"/>
			<!-- element order according B.5.3 "Property Content for Class-typed Properties" -->
			<!-- nested elements without attributes sorted by element name (not clear about the requirement) -->
			<xsl:apply-templates mode="step-4" select="node()[ not(@*) ]">
				<xsl:sort select="local-name()"/>
			</xsl:apply-templates>
			<!-- "Within the set of nested elements the order is alphabetically ordered by the value of the xmi:uuid." -->
			<xsl:apply-templates mode="step-4" select="node()[ @xmi:id ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<!-- "The set of xmi:idref elements is alphabetically ordered by the value of the xmi:idref, ..." -->
			<xsl:apply-templates mode="step-4" select="node()[ @xmi:idref ]">
				<xsl:sort select="@xmi:idref"/>
			</xsl:apply-templates>
			<!-- "... the set of href elements is alphabetically ordered by the value of the href." -->
			<xsl:apply-templates mode="step-4" select="node()[ @href ]">
				<xsl:sort select="@href"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Process other nodes -->
	<xsl:template mode="step-4" match="node()">
		<xsl:copy>
			<xsl:call-template name="AttributeOrder"/>
			<!-- element order according B.5.1 "Ordering of Elements" -->
			<xsl:apply-templates mode="step-4" select="node()">
				<xsl:sort select="local-name()"/>
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Process attributes -->
	<xsl:template mode="step-4" match="@*">
		<xsl:copy/>
	</xsl:template>
	<!--
Attribute order -->
	<xsl:template name="AttributeOrder">
		<xsl:apply-templates mode="step-4" select="@xmi:idref"/>
		<xsl:apply-templates mode="step-4" select="@href"/>
		<!-- attribute order according B.2 5. "Use XML elements for all properties except for the following XMI properties,
			which are XML attributes, and in the following order:" -->
		<xsl:apply-templates mode="step-4" select="@xmi:id"/>
		<xsl:apply-templates mode="step-4" select="@xmi:uuid"/>
		<xsl:apply-templates mode="step-4" select="@xmi:type"/>
	</xsl:template>
</xsl:stylesheet>
