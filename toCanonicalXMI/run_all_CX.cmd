set filename=%~n1
set extension=%~x1
call xalan -in "%~f1" -xsl toCanonicalXMI_step-1.xslt -out "%filename%_step-1%extension%" -param ModelName %2
rem pause
call xalan -in "%filename%_step-1%extension%" -xsl toCanonicalXMI_step-2.xslt -out "%filename%_step-2%extension%"
rem pause
call xalan -in "%filename%_step-2%extension%" -xsl toCanonicalXMI_step-3.xslt -out "%filename%_step-3%extension%" -param UniqueNames no
call xalan -in "%filename%_step-2%extension%" -xsl toCanonicalXMI_step-3.xslt -out "%filename%_step-3_UniqueNames%extension%" -param UniqueNames yes
rem pause
call xalan -in "%filename%_step-3%extension%" -xsl toCanonicalXMI_step-4.xslt -out "%filename%_step-4%extension%"
call xalan -in "%filename%_step-3_UniqueNames%extension%" -xsl toCanonicalXMI_step-4.xslt -out "%filename%_step-4_UniqueNames%extension%"
call xalan -in "%filename%_step-4_UniqueNames%extension%" -xsl toCanonicalXMI_validateIds.xslt -out "%filename%_step-4_UniqueNames_validateIds.txt"
rem pause
call saxon -xsl:format.xslt -s:%filename%_step-4%extension% -o:%filename%%extension%
call saxon -xsl:format.xslt -s:%filename%_step-4_UniqueNames%extension% -o:%filename%_UniqueNames%extension%
copy %filename%_UniqueNames%extension% %filename%_UniqueNames_Eclipse%extension%
sed -i "15,$s|http://www.omg.org/spec/UML/20131001/StandardProfile|http://www.eclipse.org/uml2/5.0.0/UML/Profile/Standard|g" %filename%_UniqueNames_Eclipse%extension%
sed -i "15,$s|http://www.omg.org/spec/UML/20131001|http://www.eclipse.org/uml2/5.0.0/UML|g" %filename%_UniqueNames_Eclipse%extension%
