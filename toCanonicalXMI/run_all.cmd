set filename=%~n1
set extension=%~x1
call xalan -in "%~f1" -xsl toCanonicalXMI_step-1.xslt -out "%filename%_step-1%extension%"
rem pause
call xalan -in "%filename%_step-1%extension%" -xsl toCanonicalXMI_step-2.xslt -out "%filename%_step-2%extension%"
rem pause
call xalan -in "%filename%_step-2%extension%" -xsl toCanonicalXMI_step-3.xslt -out "%filename%_step-3%extension%" -param UniqueAssociationNames no
call xalan -in "%filename%_step-2%extension%" -xsl toCanonicalXMI_step-3.xslt -out "%filename%_step-3_UniqueAssociationNames%extension%" -param UniqueAssociationNames yes
rem pause
call xalan -in "%filename%_step-3%extension%" -xsl toCanonicalXMI_step-4.xslt -out "%filename%_step-4%extension%"
call xalan -in "%filename%_step-3_UniqueAssociationNames%extension%" -xsl toCanonicalXMI_step-4.xslt -out "%filename%_step-4_UniqueAssociationNames%extension%"
call xalan -in "%filename%_step-4_UniqueAssociationNames%extension%" -xsl toCanonicalXMI_validateIds.xslt -out "%filename%_step-4_UniqueAssociationNames_validateIds.txt"
