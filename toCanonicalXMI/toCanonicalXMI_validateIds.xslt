<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:uml="http://www.omg.org/spec/UML/20131001" xmlns:StandardProfile="http://www.omg.org/spec/UML/20131001/StandardProfile">
	<xsl:output method="text"/>
	<!--
Key for all IDs -->
	<xsl:key name="id" match="//*[@xmi:id]" use="@xmi:id"/>
	<!--
Key for all idrefs -->
	<xsl:key name="idref" match="//*[@xmi:idref]" use="@xmi:idref"/>
	<!--
Entry point. -->
	<xsl:template match="/">
		<xsl:call-template name="checkId"/>
		<xsl:call-template name="checkIdref"/>
		<xsl:call-template name="checkIdUniqueness"/>
	</xsl:template>
	<!--
check if xmi:id is referenced
-->
	<xsl:template name="checkId">
		<xsl:text>*** check if xmi:id is referenced
</xsl:text>
		<xsl:for-each select="//@xmi:id">
			<xsl:sort select="."/>
			<xsl:if test="not( key( 'idref', . ) )">
				<xsl:choose>
					<xsl:when test="
						local-name(..) = 'lowerValue' or
						local-name(..) = 'upperValue' or
						local-name(..) = 'ownedComment' or
						local-name(..) = 'generalization' or
						local-name(..) = 'Derive' or
						local-name(..) = 'Refine' or
						local-name(..) = 'Trace'
						">
						<xsl:text>Information: </xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Warning:     </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>xmi:id not referenced </xsl:text>
				<xsl:value-of select="."/>
				<xsl:text>&#xa;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--
check if xmi:idref has related xmi:id
-->
	<xsl:template name="checkIdref">
		<xsl:text>
*** check if xmi:idref has related xmi:id
</xsl:text>
		<xsl:for-each select="//@xmi:idref">
			<xsl:sort select="."/>
			<xsl:if test="not( key( 'id', . ) )">
				<xsl:text>xmi:id not found: </xsl:text>
				<xsl:value-of select="."/>
				<xsl:text>, element: </xsl:text>
				<xsl:value-of select="name(..)"/>
				<xsl:text>, parent element id: </xsl:text>
				<xsl:value-of select="../../@xmi:id"/>
				<xsl:text>&#xa;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--
check if xmi:idref has related xmi:id
-->
	<xsl:template name="checkIdUniqueness">
		<xsl:text>
*** check if xmi:id is unique
</xsl:text>
		<xsl:for-each select="//@xmi:id">
			<xsl:sort select="."/>
			<xsl:if test="count( key( 'id', . ) ) > 1">
				<xsl:text>xmi:id is not unique: </xsl:text>
				<xsl:value-of select="."/>
				<xsl:text>, occurs: </xsl:text>
				<xsl:value-of select="count( key( 'id', . ) )"/>
				<xsl:text>&#xa;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>