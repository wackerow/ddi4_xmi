<?xml version="1.0" encoding="UTF-8"?>
<!--
Transformation to Canonical XMI according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (short CX)
	https://www.omg.org/spec/XMI/2.5.1/PDF
	from XMI export flavor of Enterprise Architect 14.1, exporter version 6.5
		Publish / Other Formats
			XML Type: UML 2.4.1 (XMI 2.4.2)
			"Export Diagrams" not checked
			"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XML.
CONFIGURE / Resources / Stylesheets (right-click)

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-4:
  Sort nodes on same level with name by @xmi:uuid according to CX

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:uml="http://www.omg.org/spec/UML/20131001" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org" xmlns:StandardProfile="http://www.omg.org/spec/UML/20131001/StandardProfile" extension-element-prefixes="exslt msxsl xalan">
	<!--
Set explicit encoding of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
Entry point. -->
	<xsl:template match="/">
		<xsl:comment>
			<![CDATA[

Namespaces are set for UML 2.5 and XMI 2.5.

For UML 2.4.1 and XMI 2.4.1: change all occurences of '20131001' to '20100901'.

For Eclipse:
  Change UML namespace from 'http://www.omg.org/spec/UML/20131001' to 'http://www.eclipse.org/uml2/5.0.0/UML'.
  Change UML namespace from 'http://www.omg.org/spec/UML/20131001/StandardProfile' to 'http://www.eclipse.org/uml2/5.0.0/UML/Profile/Standard'.

]]>
		</xsl:comment>
		<xsl:apply-templates mode="step-4" select="node()"/>
	</xsl:template>
	<!--
Process nodes -->
	<xsl:template mode="step-4" match="node()">
		<xsl:copy>
			<xsl:call-template name="AttributeOrder"/>
			<xsl:apply-templates mode="step-4" select="node()[ local-name()!='ownedAttribute' and local-name()!='ownedLiteral' and local-name()!='packagedElement' ]"/>
			<!-- sort nodes with name by @xmi:uuid -->
			<xsl:apply-templates mode="step-4" select="packagedElement[ @xmi:type='uml:Abstraction' ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="step-4" select="packagedElement[ @xmi:type='uml:Association' ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="step-4" select="packagedElement[ @xmi:type='uml:Class' ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="step-4" select="packagedElement[ @xmi:type='uml:DataType' ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="step-4" select="packagedElement[ @xmi:type='uml:Enumeration' ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="step-4" select="packagedElement[ @xmi:type='uml:Package' ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="step-4" select="packagedElement[ @xmi:type='uml:PrimitiveType' ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<xsl:apply-templates mode="step-4" select="ownedAttribute | ownedLiteral">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Attribute order -->
	<xsl:template name="AttributeOrder">
		<xsl:copy-of select="@xmi:idref | @href"/>
		<!-- attribute order according B.2 5. "Use XML elements for all properties except for the following XMI properties,
			which are XML attributes, and in the following order:" -->
		<xsl:copy-of select="@xmi:id"/>
		<xsl:copy-of select="@xmi:uuid"/>
		<xsl:copy-of select="@xmi:type"/>
	</xsl:template>
</xsl:stylesheet>