<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:uml="http://www.omg.org/spec/UML/20110701">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
parameters -->
	<xsl:param name="GlobalDdiIdentifier" select=" 'DDICDILibrary-DDICDIModels-DDICDILibrary-DataTypes-StructuredDataTypes-GlobalDdiIdentifier' "/>
	<xsl:param name="GlobalDdiIdentifierName" select=" 'GlobalDdiIdentifier' "/>
	<xsl:param name="organizationIdentifier" select=" 'organizationIdentifier' "/>
	<xsl:param name="objectIdentifier" select=" 'objectIdentifier' "/>
	<xsl:param name="versionIdentifier" select=" 'versionIdentifier' "/>
	<xsl:param name="identifierAttribute" select=" 'identifier' "/>
	<!--
Entry point -->
	<xsl:template match="/">
		<xsl:apply-templates select="@* | node()"/>
	</xsl:template>
	<!--
identity transform
-->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<!--
remove package Identification
-->
	<xsl:template match="packagedElement[@xmi:id='DDICDILibrary-Classes-Identification']"/>
	<!--
remove package Miscellaneous
-->
	<xsl:template match="packagedElement[@xmi:id='DDICDILibrary-Classes-Miscellaneous']"/>
	<!--
remove generalization to Identifiable
-->
	<xsl:template match="generalization[general/@xmi:idref='DDICDILibrary-Classes-Identification-Identifiable']"/>
	<!--
remove generalization to AnnotatedIdentifiable
-->
	<xsl:template match="generalization[general/@xmi:idref='DDICDILibrary-Classes-Identification-AnnotatedIdentifiable']"/>
	<!--
remove associations to AnnotatedIdentifiable
-->
	<xsl:template match="packagedElement[@xmi:type='uml:Association'][ownedEnd/type/@xmi:idref='DDICDILibrary-Classes-Miscellaneous-Annotation']"/>
	<!--
process class
-->
	<xsl:template match="packagedElement[@xmi:type='uml:Class']">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
			<xsl:call-template name="identifier"/>
		</xsl:copy>
	</xsl:template>
	<!--
create identifier attribute
-->
	<xsl:template name="identifier">
		<xsl:variable name="id">
			<xsl:value-of select="../@xmi:id"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$identifierAttribute"/>
			<xsl:value-of select="generate-id()"/>
		</xsl:variable>
		<!-- add new attribute -->
		<ownedAttribute xmi:id="{$id}" xmi:type="uml:Property">
			<ownedComment xmi:id="{concat($id, '-ownedComment')}" xmi:type="uml:Comment">
				<annotatedElement xmi:idref="{$id}"/>
				<body>Identifier of an agent within a specified system.</body>
			</ownedComment>
			<lowerValue xmi:id="{concat($id, '-lowerValue')}" xmi:type="uml:LiteralInteger"/>
			<upperValue xmi:id="{concat($id, '-upperValue')}" xmi:type="uml:LiteralUnlimitedNatural">
				<value>*</value>
			</upperValue>
			<name>
				<xsl:value-of select="$identifierAttribute"/>
			</name>
			<type xmi:idref="{$GlobalDdiIdentifier}"/>
		</ownedAttribute>
	</xsl:template>
	<!--
add new data types before first data type
-->
	<xsl:template match="packagedElement[@xmi:type='uml:DataType'][../@xmi:id='DDICDILibrary-DataTypes-StructuredDataTypes'][1]">
		<packagedElement xmi:id="{$GlobalDdiIdentifier}" xmi:type="uml:DataType">
			<ownedComment xmi:id="{concat($GlobalDdiIdentifier, '-ownedComment')}" xmi:type="uml:Comment">
				<annotatedElement xmi:idref="{$GlobalDdiIdentifier}"/>
				<body>Definition
==========
</body>
			</ownedComment>
			<name>
				<xsl:value-of select="$GlobalDdiIdentifierName"/>
			</name>
			<ownedAttribute xmi:id="{concat($GlobalDdiIdentifier, '-', $organizationIdentifier)}" xmi:type="uml:Property">
				<ownedComment xmi:id="{concat($GlobalDdiIdentifier, '-', $organizationIdentifier, '-ownedComment')}" xmi:type="uml:Comment">
					<annotatedElement xmi:idref="{concat($GlobalDdiIdentifier, '-', $organizationIdentifier)}"/>
					<body>organizationIdentifier</body>
				</ownedComment>
				<lowerValue xmi:id="{concat($GlobalDdiIdentifier, '-', $organizationIdentifier, '-lowerValue')}" xmi:type="uml:LiteralInteger">
					<value>1</value>
				</lowerValue>
				<upperValue xmi:id="{concat($GlobalDdiIdentifier, '-', $organizationIdentifier, '-upperValue')}" xmi:type="uml:LiteralUnlimitedNatural">
					<value>1</value>
				</upperValue>
				<name>
					<xsl:value-of select="$organizationIdentifier"/>
				</name>
				<type href="http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi#String"/>
			</ownedAttribute>
			<ownedAttribute xmi:id="{concat($GlobalDdiIdentifier, '-', $objectIdentifier)}" xmi:type="uml:Property">
				<ownedComment xmi:id="{concat($GlobalDdiIdentifier, '-', $objectIdentifier, '-ownedComment')}" xmi:type="uml:Comment">
					<annotatedElement xmi:idref="{concat($GlobalDdiIdentifier, '-', $objectIdentifier)}"/>
					<body>objectIdentifier</body>
				</ownedComment>
				<lowerValue xmi:id="{concat($GlobalDdiIdentifier, '-', $objectIdentifier, '-lowerValue')}" xmi:type="uml:LiteralInteger">
					<value>1</value>
				</lowerValue>
				<upperValue xmi:id="{concat($GlobalDdiIdentifier, '-', $objectIdentifier, '-upperValue')}" xmi:type="uml:LiteralUnlimitedNatural">
					<value>1</value>
				</upperValue>
				<name>
					<xsl:value-of select="$objectIdentifier"/>
				</name>
				<type href="http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi#String"/>
			</ownedAttribute>
			<ownedAttribute xmi:id="{concat($GlobalDdiIdentifier, '-', $versionIdentifier)}" xmi:type="uml:Property">
				<ownedComment xmi:id="{concat($GlobalDdiIdentifier, '-', $versionIdentifier, '-ownedComment')}" xmi:type="uml:Comment">
					<annotatedElement xmi:idref="{concat($GlobalDdiIdentifier, '-', $versionIdentifier)}"/>
					<body>versionIdentifier</body>
				</ownedComment>
				<lowerValue xmi:id="{concat($GlobalDdiIdentifier, '-', $versionIdentifier, '-lowerValue')}" xmi:type="uml:LiteralInteger">
					<value>1</value>
				</lowerValue>
				<upperValue xmi:id="{concat($GlobalDdiIdentifier, '-', $versionIdentifier, '-upperValue')}" xmi:type="uml:LiteralUnlimitedNatural">
					<value>1</value>
				</upperValue>
				<name>
					<xsl:value-of select="$versionIdentifier"/>
				</name>
				<type href="http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi#String"/>
			</ownedAttribute>
		</packagedElement>
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>