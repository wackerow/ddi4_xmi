<?xml version="1.0" encoding="UTF-8"?>
<!--
Transformation to Canonical XMI according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (short CX)
	https://www.omg.org/spec/XMI/2.5.1/PDF
	from XMI export flavor of Enterprise Architect 14.1, exporter version 6.5
		Publish / Other Formats
			XML Type: UML 2.4.1 (XMI 2.4.2)
			"Export Diagrams" not checked
			"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XML.
CONFIGURE / Resources / Stylesheets (right-click)

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-3:
  create xmi:id and xmi:uuid according to CX

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org" extension-element-prefixes="exslt msxsl xalan">
	<!--
Set explicit encoding of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
EA: Set ModelName to name of first package in wrapper package with name "Model" -->
	<xsl:param name="ModelName" select="/xmi:XMI/uml:Model[ @xmi:type='uml:Model' ][ @name='EA_Model' ]/packagedElement[ @xmi:type='uml:Package' ][ @name='Model' ]/packagedElement[ @xmi:type='uml:Package' ]/@name"/>
	<!--
Set DDI4_XMI_URI_Root -->
	<xsl:param name="DDI4_XMI_URI_Root" select=" 'http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PIM.xmi' "/>
	<!--
Set PrimitiveTypes URI-->
	<xsl:param name="PrimitiveTypesURI" select=" 'http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi' "/>
	<!--
Key for all IDs -->
	<xsl:key name="id" match="//*[@xmi:id]" use="@xmi:id"/>
	<!--
Entry point. -->
	<xsl:template match="/">
		<xsl:apply-templates mode="step-3" select="node()"/>
	</xsl:template>
	<!--
Process nodes -->
	<xsl:template mode="step-3" match="node()">
		<xsl:copy>
			<xsl:apply-templates mode="step-3" select="@*"/>
			<xsl:apply-templates mode="step-3" select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process attributes -->
	<xsl:template mode="step-3" match="@*">
		<xsl:copy/>
	</xsl:template>
	<!--
Process attribute id -->
	<xsl:template mode="step-3" match="@xmi:id">
		<xsl:variable name="IdValue">
			<xsl:call-template name="CreateID">
				<xsl:with-param name="Node" select="."/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="UuidValue" select="concat( $DDI4_XMI_URI_Root, '#', $IdValue)"/>
		<xsl:attribute name="xmi:id"><xsl:value-of select="$IdValue"></xsl:value-of></xsl:attribute>
		<xsl:attribute name="xmi:uuid"><xsl:value-of select="$UuidValue"></xsl:value-of></xsl:attribute>
	</xsl:template>
	<!--
Process attribute idref -->
	<xsl:template mode="step-3" match="@xmi:idref">
		<xsl:variable name="IdValue">
			<xsl:call-template name="CreateID">
				<xsl:with-param name="Node" select="key( 'id', . )"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:attribute name="xmi:idref"><xsl:value-of select="$IdValue"></xsl:value-of></xsl:attribute>
	</xsl:template>
	<!--
Create id according CX -->
	<xsl:template name="CreateID">
		<xsl:param name="Node"/>
		<!-- set node environment -->
		<xsl:for-each select="$Node">
			<!-- without root node -->
			<xsl:for-each select="ancestor-or-self::*[parent::*]">
				<xsl:variable name="Name" select="name"/>
				<xsl:choose>
					<xsl:when test="name">
						<xsl:value-of select="name"/>
						<!-- deal with associations which have non-unique names in the same package -->
						<xsl:if test=" @xmi:type='uml:Association' and count( following-sibling::packagedElement[ @xmi:type='uml:Association' ][ name=$Name ] ) > 0 ">
							<xsl:variable name="SequenceNumber" select="count( preceding-sibling::packagedElement[ @xmi:type='uml:Association' ][ name=$Name ] ) + 1"/>
							<!-- add sequence number -->
							<xsl:text>-</xsl:text>
							<xsl:value-of select="$SequenceNumber"/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="local-name()"/>
						<!-- deal with ownedAttribute without names -->
						<xsl:if test=" local-name()='ownedAttribute' ">
							<xsl:variable name="SequenceNumber" select="count( preceding-sibling::ownedAttribute ) + 1"/>
							<!-- add sequence number -->
							<xsl:text>-</xsl:text>
							<xsl:value-of select="$SequenceNumber"/>
						</xsl:if>
						<!-- deal with association without names -->
						<xsl:if test=" local-name()='packagedElement' and @xmi:type='uml:Association' ">
							<xsl:variable name="SequenceNumber" select="count( preceding-sibling::packagedElement[ @xmi:type='uml:Association' ] ) + 1"/>
							<!-- add sequence number -->
							<xsl:text>-</xsl:text>
							<xsl:value-of select="$SequenceNumber"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="not( position()=last() )">
					<xsl:text>-</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
