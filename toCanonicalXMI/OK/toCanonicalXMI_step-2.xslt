<?xml version="1.0" encoding="UTF-8"?>
<!--
Transformation to Canonical XMI according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (short CX)
	https://www.omg.org/spec/XMI/2.5.1/PDF
	from XMI export flavor of Enterprise Architect 14.1, exporter version 6.5
		Publish / Other Formats
			XML Type: UML 2.4.1 (XMI 2.4.2)
			"Export Diagrams" not checked
			"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XML.
CONFIGURE / Resources / Stylesheets (right-click)

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-2:
  Add EA documentation.
  Process only elements and properties from defined UML subset.
  Transform XML attributes to XML elements except xmi:id, xmi:uui, and xmi:type according CX B.2 5.
  Remove values if they are default values.
  Set correct URIs for UML primitives

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org" extension-element-prefixes="exslt msxsl xalan">
	<!--
Set explicit encoding of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
Set DDI4_XMI_URI_Root -->
	<xsl:param name="DDI4_XMI_URI_Root" select=" 'http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PIM.xmi' "/>
	<!--
Set PrimitiveTypes URI-->
	<xsl:param name="PrimitiveTypesURI" select=" 'http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi' "/>
	<!--
Set tool name -->
	<xsl:param name="Tool">
		<xsl:choose>
			<xsl:when test=" /xmi:XMI/xmi:Documentation/@exporter = 'Enterprise Architect' ">
				<xsl:text>EnterpriseArchitect</xsl:text>
			</xsl:when>
			<xsl:when test=" /xmi:XMI/uml:Model[2]/@name = 'RhapsodyStandardModel' ">
				<xsl:text>IBMRhapsody</xsl:text>
			</xsl:when>
			<xsl:when test=" /xmi:XMI/xmi:documentation/xmi:exporter = 'MagicDraw Clean XMI Exporter' ">
				<xsl:text>MagicDraw</xsl:text>
			</xsl:when>
			<xsl:when test="contains( /xmi:XMI/xmi:Documentation/exporter, 'Altova' )">
				<xsl:text>Umodel</xsl:text>
			</xsl:when>
			<xsl:when test="/xmi:XMI/uml:Model/xmi:Documentation/@exporter = 'Visual Paradigm' ">
				<xsl:text>VisualParadigm</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:param>
	<!--
EA: Key for documentation in extension
-->
	<xsl:key name="EADoc" match="/xmi:XMI/xmi:Extension/elements/element | /xmi:XMI/xmi:Extension/elements/element/attributes/attribute" use="@xmi:idref"/>
	<!--
Entry point. -->
	<xsl:template match="/">
		<xsl:apply-templates mode="step-2" select="xmi:XMI"/>
	</xsl:template>
	<!--
Process XMI element
-->
	<xsl:template mode="step-2" match="xmi:XMI">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="uml:Model"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process Model
-->
	<xsl:template mode="step-2" match="uml:Model">
		<xsl:variable name="Name" select="@name | name"/>
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@name | name | ownedComment | packagedElement">
				<xsl:with-param name="Name" select="$Name"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Process packagedElement
-->
	<xsl:template mode="step-2" match="packagedElement">
		<xsl:variable name="Name" select="@name | name"/>
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="generalization | @isAbstract | isAbstract | @name | name | @memberEnd | memberEnd | ownedAttribute | ownedComment | ownedEnd | packagedElement | ownedLiteral">
				<xsl:with-param name="Name" select="$Name"/>
			</xsl:apply-templates>
			<!-- if Enterprise Architect: add documentation from extension -->
			<xsl:if test=" $Tool = 'EnterpriseArchitect' ">
				<xsl:call-template name="EADocumentation">
					<xsl:with-param name="Documentation" select="key( 'EADoc', @xmi:id )/properties/@documentation"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--
Process generalization in packagedElement
-->
	<xsl:template mode="step-2" match="generalization">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@general | general | ownedComment"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process general in generalization
-->
	<xsl:template mode="step-2" match="@general">
		<xsl:element name="general">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="general">
		<xsl:copy>
			<xsl:attribute name="xmi:idref"><xsl:value-of select="@xmi:idref | @href"/></xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<!--
Process isAbstract in packagedElement
-->
	<xsl:template mode="step-2" match="@isAbstract | isAbstract">
		<xsl:if test=" . = 'true' ">
			<xsl:element name="isAbstract">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process ownedAttribute in packagedElement
-->
	<xsl:template mode="step-2" match="ownedAttribute">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@association | association | @isID | isID | lowerValue | @name | name | ownedComment | @type | type | upperValue"/>
			<!-- if Enterprise Architect: add documentation from extension -->
			<xsl:if test=" $Tool = 'EnterpriseArchitect' ">
				<xsl:call-template name="EADocumentation">
					<xsl:with-param name="Documentation" select="key( 'EADoc', @xmi:id )/documentation/@value"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--
Process association in ownedAttribute
-->
	<xsl:template mode="step-2" match="@association">
		<xsl:element name="association">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="association">
		<xsl:element name="association">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="@xmi:idref | @href"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--
Process isID in ownedAttribute
-->
	<xsl:template mode="step-2" match="@isID | isID">
		<xsl:if test=" . = 'true' ">
			<xsl:element name="isID">
				<xsl:text>true</xsl:text>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process lowerValue in ownedAttribute
-->
	<xsl:template mode="step-2" match="lowerValue">
		<!-- nothing set if 1, is default for lowerValue -->
		<xsl:if test=" ( @value and @value != '1' ) or ( value and value != '1' ) or not( @value and value ) ">
			<xsl:copy>
				<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
				<xsl:apply-templates mode="step-2" select="@value | value"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--
Process upperValue in ownedAttribute
-->
	<xsl:template mode="step-2" match="upperValue">
		<xsl:variable name="Value" select="@value | value"/>
		<!-- nothing set if 1, is default for upperValue -->
		<!--
		<xsl:if test=" @value and @value != '1' or value and value != '1' ">
-->
		<xsl:element name="upperValue">
			<xsl:apply-templates mode="step-2" select="@xmi:id"/>
			<xsl:choose>
				<xsl:when test=" $Value = '*' ">
					<xsl:attribute name="xmi:type"><xsl:text>uml:LiteralUnlimitedNatural</xsl:text></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="xmi:type"><xsl:text>uml:LiteralInteger</xsl:text></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates mode="step-2" select="@value | value"/>
		</xsl:element>
		<!--
		</xsl:if>
-->
	</xsl:template>
	<!--
Process value in lowerValue
-->
	<xsl:template mode="step-2" match="@value | value">
		<!-- nothing set if 0, is default for value-->
		<xsl:if test="not( local-name(..) = 'lowerValue' and . = '0' )">
			<xsl:element name="value">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process name in packagedElement
-->
	<xsl:template mode="step-2" match="@name | name">
		<xsl:element name="name">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>
	<!--
Process memberEnd in packagedElement
-->
	<xsl:template mode="step-2" match="@memberEnd">
		<!-- if two references like in Eclipse -->
		<xsl:choose>
			<xsl:when test="contains( ., ' ' )">
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref"><xsl:value-of select="substring-before( ., ' ' )"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref"><xsl:value-of select="substring-after( ., ' ' )"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="memberEnd">
					<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="step-2" match="memberEnd">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:idref"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process ownedEnd in packagedElement
-->
	<xsl:template mode="step-2" match="ownedEnd">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates select="@association | association | lowerValue | ownedComment | @type | type | upperValue" mode="step-2"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process type in ownedEnd
-->
	<xsl:template mode="step-2" match="@type">
		<xsl:call-template name="type">
			<xsl:with-param name="Value" select="."/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="step-2" match="type">
		<xsl:variable name="Value">
			<xsl:choose>
				<xsl:when test="@href">
					<xsl:value-of select="@href"/>
				</xsl:when>
				<xsl:when test="@xmi:idref">
					<xsl:value-of select="@xmi:idref"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="type">
			<xsl:with-param name="Value" select="$Value"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="type">
		<xsl:param name="Value"/>
		<xsl:variable name="Reference">
			<!-- Determine appropriate reference. The tools have a large variety regarding the UML primitives -->
			<xsl:choose>
				<!-- if any external reference to a primitive like with canonical, eclipse, IBM Rhapsody, MagicDraw, Modelio -->
				<xsl:when test="contains( $Value, '//' ) and contains( $Value, '#' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="substring-after( $Value, '#' )"/>
				</xsl:when>
				<!-- if internal references to primitives of Astah -->
				<xsl:when test="contains( key( 'id', $Value )/../../@name, 'Primitive Package' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $Value )/../@name"/>
				</xsl:when>
				<!-- if internal references to primitives of Enterprise Architect  -->
				<xsl:when test=" $Value = 'EAJava_Boolean' or  $Value = 'EAJava_Integer' or  $Value = 'EAJava_Real' or  $Value = 'EAJava_String' or  $Value = 'EAJava_UnlimitedNatural' ">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $Value )/../@name"/>
				</xsl:when>
				<!-- if internal references to primitives of UModel -->
				<xsl:when test="contains( key( 'id', $Value )/../../@name, 'PrimitiveTypes' )">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="key( 'id', $Value )/../@name"/>
				</xsl:when>
				<!-- if internal references to primitives of Visual Paradigm -->
				<xsl:when test=" $Value = 'boolean_id' or  $Value = 'int_id' or  $Value = 'Real_id' or  $Value = 'string_id' or  $Value = 'UnlimitedNatural_id' ">
					<xsl:value-of select="$PrimitiveTypesURI"/>
					<xsl:text>#</xsl:text>
					<xsl:variable name="Name" select="key( 'id', $Value )/../@name"/>
					<xsl:choose>
						<xsl:when test=" $Name = 'boolean' ">
							<xsl:text>Boolean</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'int' ">
							<xsl:text>Integer</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'Real' ">
							<xsl:text>Real</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'string' ">
							<xsl:text>String</xsl:text>
						</xsl:when>
						<xsl:when test=" $Name = 'UnlimitedNatural' ">
							<xsl:text>UnlimitedNatural</xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:when>
				<!-- regular internal reference -->
				<xsl:otherwise>
					<xsl:value-of select="$Value"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:element name="type">
			<xsl:choose>
				<xsl:when test="starts-with( $Reference, 'http://' )">
					<xsl:attribute name="href"><xsl:value-of select="$Reference"/></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="xmi:idref"><xsl:value-of select="$Reference"/></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<!--
Process ownedComment in packagedElement
-->
	<xsl:template mode="step-2" match="ownedComment">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@annotatedElement | annotatedElement | @body | body"/>
		</xsl:copy>
	</xsl:template>
	<!--
Process body in ownedComment
-->
	<xsl:template mode="step-2" match="@body | body">
		<xsl:element name="body">
			<xsl:value-of select="."/>
		</xsl:element>
		<!-- set reference to parent packagedElement if annotation reference does not exist -->
		<xsl:if test="not( ../@annotatedElement ) and not( ../annotatedElement )">
			<xsl:element name="annotatedElement">
				<xsl:attribute name="xmi:idref"><xsl:value-of select="parent::ownedComment/parent::*/@xmi:id"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--
Process annotatedElement in ownedComment
-->
	<xsl:template mode="step-2" match="@annotatedElement">
		<xsl:element name="annotatedElement">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="."/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template mode="step-2" match="annotatedElement">
		<xsl:element name="annotatedElement">
			<xsl:attribute name="xmi:idref"><xsl:value-of select="@xmi:idref | @href"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--
Process ownedLiteral in packagedElement
-->
	<xsl:template mode="step-2" match="ownedLiteral">
		<xsl:copy>
			<xsl:apply-templates mode="step-2" select="@xmi:id | @xmi:type"/>
			<xsl:apply-templates mode="step-2" select="@annotatedElement | annotatedElement | @body | body | @name | name"/>
		</xsl:copy>
	</xsl:template>
	<!--
Enterprise Architect: documentation
-->
	<xsl:template name="EADocumentation">
		<xsl:param name="Documentation"/>
		<xsl:if test="$Documentation">
			<ownedComment xmi:id="dummy" xmi:type="uml:Comment">
				<annotatedElement xmi:idref="{@xmi:id}"/>
				<body>
					<xsl:value-of select="$Documentation"/>
				</body>
			</ownedComment>
		</xsl:if>
	</xsl:template>
	<!--
copy attributes
-->
	<xsl:template mode="step-2" match="@*">
		<xsl:copy/>
	</xsl:template>
</xsl:stylesheet>
