<?xml version="1.0" encoding="UTF-8"?>
<!--
Transformation to Canonical XMI according "Annex B Canonical XMI" of "XML Metadata Interchange (XMI) Specification Version 2.5.1" (short CX)
	https://www.omg.org/spec/XMI/2.5.1/PDF
	from XMI export flavor of Enterprise Architect 14.1, exporter version 6.5
		Publish / Other Formats
			XML Type: UML 2.4.1 (XMI 2.4.2)
			"Export Diagrams" not checked
			"Exclude EA Extensions" not checked

Notes below: EA - is specific to Enterprise Architect

The XSLT can be imported into Enterprise Architect and used in the publish command for post processing the XML.
CONFIGURE / Resources / Stylesheets (right-click)

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Step-4:
  Sort elements according to CX

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org" extension-element-prefixes="exslt msxsl xalan">
	<!--
Set explicit encoding of UTF-8
According to Annex B Canonical XMI, B2.1. -->
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>
	<!--
Entry point. -->
	<xsl:template match="/">
		<xsl:apply-templates mode="step-4" select="node()"/>
	</xsl:template>
	<!--
Process nodes and sort -->
	<xsl:template mode="step-4" match="node()[ @xmi:type='uml:Class' or @xmi:type='uml:Property' ]">
		<xsl:copy>
			<xsl:call-template name="AttributeOrder"/>
			<!-- element order according B.5.3 "Property Content for Class-typed Properties" -->
			<!-- nested elements without attributes sorted by element name (not clear about the requirement) -->
			<xsl:apply-templates mode="step-4" select="node()[ not(@*) ]">
				<xsl:sort select="local-name()"/>
			</xsl:apply-templates>
			<!-- "Within the set of nested elements the order is alphabetically ordered by the value of the xmi:uuid." -->
			<xsl:apply-templates mode="step-4" select="node()[ @xmi:id ]">
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
			<!-- "The set of xmi:idref elements is alphabetically ordered by the value of the xmi:idref, ..." -->
			<xsl:apply-templates mode="step-4" select="node()[ @xmi:idref ]">
				<xsl:sort select="@xmi:idref"/>
			</xsl:apply-templates>
			<!-- "... the set of href elements is alphabetically ordered by the value of the href." -->
			<xsl:apply-templates mode="step-4" select="node()[ @hdref ]">
				<xsl:sort select="@href"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Process other nodes -->
	<xsl:template mode="step-4" match="node()">
		<xsl:copy>
			<xsl:call-template name="AttributeOrder"/>
			<!-- element order according B.5.1 "Ordering of Elements" -->
			<xsl:apply-templates mode="step-4" select="node()">
				<xsl:sort select="local-name()"/>
				<xsl:sort select="@xmi:uuid"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!--
Process attributes -->
	<xsl:template mode="step-4" match="@*">
		<xsl:copy/>
	</xsl:template>
	<!--
Attribute order -->
	<xsl:template name="AttributeOrder">
		<xsl:apply-templates mode="step-4" select="@xmi:idref"/>
		<xsl:apply-templates mode="step-4" select="@href"/>
		<!-- attribute order according B.2 5. "Use XML elements for all properties except for the following XMI properties,
			which are XML attributes, and in the following order:" -->
		<xsl:apply-templates mode="step-4" select="@xmi:id"/>
		<xsl:apply-templates mode="step-4" select="@xmi:uuid"/>
		<xsl:apply-templates mode="step-4" select="@xmi:type"/>
	</xsl:template>
</xsl:stylesheet>
