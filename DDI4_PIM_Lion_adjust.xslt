<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:mofext="http://www.omg.org/spec/MOF/20110701">
	<!-- - - - - - - - - - - -->
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<!-- - - - - - - - - - - -->
	<xsl:key name="ownedAttribute_association" match="/xmi:XMI/uml:Model//ownedAttribute[ @association ]" use="@association"/>
	<xsl:key name="ownedEnd_association" match="/xmi:XMI/uml:Model//ownedEnd[ @association ]" use="@association"/>
	<xsl:key name="attributeIdref" match="/xmi:XMI/xmi:Extension//attribute[ @xmi:idref ]" use="@xmi:idref"/>
	<!-- - - - - - - - - - - -->
	<xsl:param name="ModelName" select=" 'DDI4_PIM' "/>
	<xsl:param name="PrimitiveTypesURI" select=" 'http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi' "/>
	<!-- - - - - - - - - - - -->
	<!-- identity transform -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- set namespaces version for 2.4.1 -->
	<xsl:template match="xmi:XMI">
		<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:uml="http://www.omg.org/spec/UML/20110701">
			<!--
		<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:mofext="http://www.omg.org/spec/MOF/20110701">
		-->
			<xsl:apply-templates select="node()"/>
			<!--
			<xsl:call-template name="MOF"/>
			-->
		</xmi:XMI>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- set model id and name -->
	<xsl:template match="uml:Model">
		<uml:Model xmi:type="uml:Model" xmi:id="{$ModelName}" name="{$ModelName}">
			<xsl:apply-templates select="node()"/>
			<!--
			<xsl:call-template name="XSDLibrary"/>
			-->
		</uml:Model>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- set class library id and name -->
	<xsl:template match="packagedElement[ @xmi:type='uml:Package' ][ @xmi:id='ddi4_model' ]">
		<packagedElement xmi:type="uml:Package" xmi:id="ClassLibrary" name="ClassLibrary">
			<!--
			<xsl:call-template name="DummyPackage"/>
			-->
			<xsl:apply-templates select="packagedElement[ @xmi:type='uml:Package' ]">
				<xsl:sort select="@name"/>
			</xsl:apply-templates>
		</packagedElement>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- set model version -->
	<xsl:template match="packagedElement[ @xmi:type='uml:Enumeration' ][ @xmi:id='DDI4Version' ]">
		<packagedElement xmi:type="uml:DataType" xmi:id="DDI4Version" name="DDI4Version">
			<ownedAttribute xmi:type="uml:Property" xmi:id="DDI4VersionValue" name="value" isReadOnly="true">
				<type href="http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi#String"/>
				<defaultValue xmi:type="uml:LiteralString" xmi:id="DDI4VersionValueDefault" name="default" value="{ownedLiteral/@name}"/>
			</ownedAttribute>
		</packagedElement>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- set property modifier id -->
	<xsl:template match="ownedAttribute[ @xmi:id='Identifiable_agency' ]">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="isID"><xsl:text>true</xsl:text></xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ownedAttribute[ @xmi:id='Identifiable_id' ]">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="isID"><xsl:text>true</xsl:text></xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ownedAttribute[ @xmi:id='Identifiable_version' ]">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="isID"><xsl:text>true</xsl:text></xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- add names to association ends, these are roles, right? -->
	<!--
	<xsl:template match="ownedEnd[ @association ][ not( @name ) ] | ownedAttribute[ @association ][ not( @name ) ]">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="name"><xsl:value-of select="@xmi:id"/></xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	-->
	<!-- - - - - - - - - - - -->
	<!-- add names to lower/upper values -->
	<!--
	<xsl:template match="lowerValue[ not( @name ) ] | upperValue[ not( @name ) ]">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="name"><xsl:value-of select="@xmi:id"/></xsl:attribute>
		</xsl:copy>
	</xsl:template>
	-->
	<!-- - - - - - - - - - - -->
	<!-- add reference to commented item to comment -->
	<xsl:template match="ownedComment">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="xmi:id"><xsl:value-of select="../@xmi:id"/><xsl:text>-ownedComment</xsl:text></xsl:attribute>
			<xsl:apply-templates select="node()"/>
			<annotatedElement xmi:idref="{../@xmi:id}"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- remove pipe characters in documentation strings -->
	<xsl:template match="body">
		<body>
			<xsl:call-template name="string-replace-all">
				<xsl:with-param name="text" select="."/>
				<xsl:with-param name="replace" select=" '|' "/>
				<xsl:with-param name="by" select=" '' "/>
			</xsl:call-template>
		</body>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- remove documentation element -->
	<xsl:template match="xmi:Documentation"/>
	<!-- - - - - - - - - - - -->
	<!-- remove XML comments -->
	<xsl:template match="comment()"/>
	<!-- - - - - - - - - - - -->
	<!-- change multiplicity value for infinity -->
	<xsl:template match="@value[ . = '-1' ]">
		<xsl:attribute name="value"><xsl:text>*</xsl:text></xsl:attribute>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- remove visibility attribute, this means the visibility is set to default "public" -->
	<xsl:template match="@visibility"/>
	<!-- - - - - - - - - - - -->
	<!-- set correct URI for UML primitive types -->
	<xsl:template match="@href[ contains( ., 'http://schema.omg.org/spec/UML/2.1/uml.xml' ) ]">
		<xsl:variable name="Href">
			<xsl:value-of select="$PrimitiveTypesURI"/>
			<xsl:text>#</xsl:text>
			<xsl:value-of select="substring-after( ., '#' )"/>
		</xsl:variable>
		<xsl:attribute name="href"><xsl:value-of select="$Href"/></xsl:attribute>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- set name of associations, ISSUE: name show up in UML tools but xmi:label not possible -->
	<!--
	<xsl:template match="//packagedElement[ @xmi:type='uml:Association' ]">
		<packagedElement xmi:type="uml:Association" xmi:id="{@xmi:id}" name="{@xmi:id}">
			<xsl:apply-templates select="node()"/>
		</packagedElement>
	</xsl:template>
	-->
	<!-- - - - - - - - - - - -->
	<!-- package FunctionalViews -->
	<xsl:template match="packagedElement[ @xmi:type='uml:Package' ][ @xmi:id='ddi4_views' ]">
		<packagedElement xmi:type="uml:Package" xmi:id="FunctionalViews" name="FunctionalViews">
			<xsl:apply-templates select="packagedElement[ @xmi:type='uml:Package' ]" mode="view">
				<xsl:sort select="@name"/>
			</xsl:apply-templates>
		</packagedElement>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- build view -->
	<xsl:template match="packagedElement" mode="view">
		<xsl:variable name="DiagramName">
			<xsl:value-of select="@name"/>
			<xsl:text>Diagram</xsl:text>
		</xsl:variable>
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
			<xsl:apply-templates select="/xmi:XMI/xmi:Extension/diagrams/diagram[ @xmi:id=$DiagramName ]/elements/element" mode="view">
				<xsl:with-param name="ViewName" select="@name"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- reference classes for views -->
	<xsl:template match="element" mode="view">
		<xsl:param name="ViewName"/>
		<xsl:variable name="Id">
			<xsl:value-of select="$ViewName"/>
			<xsl:text>_</xsl:text>
			<xsl:value-of select="@subject"/>
		</xsl:variable>
		<elementImport xmi:id="{$Id}" xmi:type="uml:ElementImport">
			<importedElement xmi:idref="{@subject}"/>
		</elementImport>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- add property comment from extension -->
	<xsl:template match="ownedAttribute[ @xmi:type='uml:Property' ][ not( @association ) ]">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<ownedComment xmi:type="uml:Comment">
				<xsl:attribute name="xmi:id"><xsl:value-of select="@xmi:id"/><xsl:text>-ownedComment</xsl:text></xsl:attribute>
				<body>
					<xsl:value-of select="key( 'attributeIdref', @xmi:id )/documentation/@value"/>
				</body>
				<annotatedElement xmi:idref="{@xmi:id}"/>
			</ownedComment>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- swap association ends for ownedAttribute -->
	<xsl:template match="ownedAttribute[ @xmi:type='uml:Property' ][ @association ]">
		<xsl:variable name="ownedEnd_association" select="key( 'ownedEnd_association', @association )"/>
		<xsl:variable name="LowerValueId" select="lowerValue/@xmi:id"/>
		<xsl:variable name="LowerValue" select="$ownedEnd_association/lowerValue/@value"/>
		<xsl:variable name="LowerValueType" select="$ownedEnd_association/lowerValue/@xmi:type"/>
		<xsl:variable name="UpperValueId" select="upperValue/@xmi:id"/>
		<xsl:variable name="UpperValue" select="$ownedEnd_association/upperValue/@value"/>
		<xsl:variable name="UpperValue2">
			<xsl:choose>
				<xsl:when test="$UpperValue='-1' ">
					<xsl:text>*</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$UpperValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="UpperValueType" select="$ownedEnd_association/upperValue/@xmi:type"/>
		<xsl:variable name="Aggregation" select="$ownedEnd_association/@aggregation"/>
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<!-- if aggregation or composition -->
			<xsl:if test="$Aggregation">
				<!-- add @aggregation -->
				<xsl:attribute name="aggregation"><xsl:value-of select="$Aggregation"/></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="type"/>
			<lowerValue xmi:type="{$LowerValueType}" xmi:id="{$LowerValueId}">
				<xsl:if test="$LowerValue">
					<xsl:attribute name="value"><xsl:value-of select="$LowerValue"/></xsl:attribute>
				</xsl:if>
			</lowerValue>
			<upperValue xmi:type="{$UpperValueType}" xmi:id="{$UpperValueId}" value="{$UpperValue2}"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- swap association ends for ownedEnd -->
	<xsl:template match="ownedEnd[ @xmi:type='uml:Property' ][ @association ]">
		<xsl:variable name="ownedAttribute_association" select="key( 'ownedAttribute_association', @association )"/>
		<xsl:variable name="LowerValueId" select="lowerValue/@xmi:id"/>
		<xsl:variable name="LowerValue" select="$ownedAttribute_association/lowerValue/@value"/>
		<xsl:variable name="LowerValueType" select="$ownedAttribute_association/lowerValue/@xmi:type"/>
		<xsl:variable name="UpperValueId" select="upperValue/@xmi:id"/>
		<xsl:variable name="UpperValue" select="$ownedAttribute_association/upperValue/@value"/>
		<xsl:variable name="UpperValue2">
			<xsl:choose>
				<xsl:when test="$UpperValue='-1' ">
					<xsl:text>*</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$UpperValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="UpperValueType" select="$ownedAttribute_association/upperValue/@xmi:type"/>
		<xsl:variable name="Aggregation" select="@aggregation"/>
		<xsl:copy>
			<!-- if aggregation or composition -->
			<xsl:choose>
				<xsl:when test="$Aggregation">
					<!-- remove @aggregation -->
					<xsl:apply-templates select="@xmi:id | @xmi:type | @association"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="@*"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="type"/>
			<lowerValue xmi:type="{$LowerValueType}" xmi:id="{$LowerValueId}">
				<xsl:if test="$LowerValue">
					<xsl:attribute name="value"><xsl:value-of select="$LowerValue"/></xsl:attribute>
				</xsl:if>
			</lowerValue>
			<upperValue xmi:type="{$UpperValueType}" xmi:id="{$UpperValueId}" value="{$UpperValue2}"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- remove association role names -->
	<xsl:template match="ownedAttribute[ @xmi:type='uml:Property' ][ @association ]/@name"/>
	<!-- - - - - - - - - - - -->
	<!-- remove type attribute for primitive types -->
	<xsl:template match="type[ @href ]">
		<xsl:copy>
			<xsl:apply-templates select="@href"/>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- XSD anyURI -->
	<xsl:template match="type[ @xmi:type='xsd:anyURI' ]">
		<xsl:copy>
			<xsl:attribute name="xmi:idref"><xsl:text>XMLSchemaDatatypes-anyURI</xsl:text></xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- XSD language -->
	<xsl:template match="@xmi:type[ .='xs:language' ]">
		<xsl:attribute name="xmi:idref"><xsl:text>XMLSchemaDatatypes-language</xsl:text></xsl:attribute>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- add ids to enumerations -->
	<xsl:template match="ownedLiteral[ @xmi:type='uml:EnumerationLiteral' ]">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:variable name="Id">
				<xsl:value-of select="../@name"/>
				<xsl:text>_</xsl:text>
				<xsl:value-of select="@name"/>
			</xsl:variable>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$Id"/></xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- remove XMI extension -->
	<xsl:template match="xmi:Extension"/>
	<!-- - - - - - - - - - - -->
	<!-- remove empty comments to avoid "<body/>" after beautify -->
	<xsl:template match="ownedComment[ @xmi:type='uml:Comment' ] [normalize-space( body ) = '' ]"/>
	<!-- - - - - - - - - - - -->
	<!-- XML Schema datatypes, workaround -->
	<xsl:template match="packagedElement[ @xmi:type='uml:Package' ][ @xmi:id='Primitives' ]">
		<packagedElement xmi:id="XMLSchemaDatatypes" xmi:type="uml:Package">
			<ownedComment xmi:id="XMLSchemaDatatypes-ownedComment" xmi:type="uml:Comment">
				<body>This package comprehends XML Schema datatypes which are used in the DDI model.</body>
				<annotatedElement xmi:idref="XMLSchemaDatatypes"/>
			</ownedComment>
			<packagedElement xmi:id="XMLSchemaDatatypes-anyURI" xmi:type="uml:PrimitiveType">
				<ownedComment xmi:id="XMLSchemaDatatypes-anyURI-ownedComment" xmi:type="uml:Comment">
					<body>anyURI is like the primitive datatype "anyURI" of the XML Schema datatypes.

Definition from XML Schema Part 2: Datatypes Second Edition:
"anyURI" represents a Uniform Resource Identifier Reference (URI). An "anyURI" value can be absolute or relative, and may have an optional fragment identifier (i.e., it may be a URI Reference). This type should be used to specify the intention that the value fulfills the role of a URI as defined by [RFC 2396], as amended by [RFC 2732].

For details see at https://www.w3.org/TR/xmlschema-2/#anyURI</body>
					<annotatedElement xmi:idref="XMLSchemaDatatypes-anyURI"/>
				</ownedComment>
				<name>anyURI</name>
			</packagedElement>
			<packagedElement xmi:id="XMLSchemaDatatypes-language" xmi:type="uml:PrimitiveType">
				<ownedComment xmi:id="XMLSchemaDatatypes-language-ownedComment" xmi:type="uml:Comment">
					<body>language is like the derived datatype "language" of the XML Schema datatypes.

Definition from XML Schema Part 2: Datatypes Second Edition:
"language" represents natural language identifiers as defined by by [RFC 3066]. The value space of "language" is the set of all strings that are valid language identifiers as defined [RFC 3066] . The lexical space of language is the set of all strings that conform to the pattern [a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*. The base type of "language" is token.

For details see at https://www.w3.org/TR/xmlschema-2/#language.</body>
					<annotatedElement xmi:idref="XMLSchemaDatatypes-language"/>
				</ownedComment>
				<name>language</name>
			</packagedElement>
			<name>XMLSchemaDatatypes</name>
			<URI>http://www.ddialliance.org/XMLSchemaDatatypes</URI>
		</packagedElement>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- include OMG XSDLibrary -->
	<xsl:template name="XSDLibrary">
		<packagedElement xmi:id="DDI4_PIM-OtherPackages" xmi:uuid="http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PIM.xmi#DDI4_PIM-OtherPackages" xmi:type="uml:Package">
			<packageImport xmi:type="uml:PackageImport" xmi:id="XSDLibraryImport" xmi:uuid="http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PIM.xmi#DDI4_PIM-OtherPackages-XSDLibraryImport">
				<importedPackage href="ODM/XSDLibrary.xmi#_16_0_1_2780137_1234909558376_738774_2777"/>
			</packageImport>
			<name>OtherPackages</name>
		</packagedElement>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- MOF annotations for datatypes -->
	<xsl:template name="MOF">
		<mofext:Tag xmi:id="anyURI_Tag" xmi:type="mofext:Tag">
			<name>org.omg.xmi.schemaType</name>
			<value>http://www.w3.org/2001/XMLSchema#anyURI</value>
			<element>anyURI</element>
		</mofext:Tag>
		<mofext:Tag xmi:id="language_Tag" xmi:type="mofext:Tag">
			<name>org.omg.xmi.schemaType</name>
			<value>http://www.w3.org/2001/XMLSchema#language</value>
			<element>language</element>
		</mofext:Tag>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<!-- utility templates -->
	<xsl:template name="string-replace-all">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="by"/>
		<xsl:choose>
			<xsl:when test="$text = '' or $replace = ''or not($replace)">
				<!-- Prevent this routine from hanging -->
				<xsl:value-of select="$text"/>
			</xsl:when>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text,$replace)"/>
				<xsl:value-of select="$by"/>
				<xsl:call-template name="string-replace-all">
					<xsl:with-param name="text" select="substring-after($text,$replace)"/>
					<xsl:with-param name="replace" select="$replace"/>
					<xsl:with-param name="by" select="$by"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- - - - - - - - - - - -->
</xsl:stylesheet>
