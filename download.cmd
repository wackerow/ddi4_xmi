@echo off
wget http://lion.ddialliance.org/xmi.xml -O DDI4_PIM_Lion.xmi
if errorlevel 1 (
  echo Download not successful
) else (
  date/t > download_timestamp.txt
  time/t >> download_timestamp.txt
)
