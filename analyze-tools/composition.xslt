<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:mofext="http://www.omg.org/spec/MOF/20110701">
	<!-- - - - - - - - - - - -->
	<xsl:output method="text" encoding="UTF-8"/>
	<!-- - - - - - - - - - - -->
	<xsl:key name="Id" match="/xmi:XMI/uml:Model//*" use="@xmi:id"/>
	<xsl:key name="Idref" match="/xmi:XMI/uml:Model//*" use="@xmi:idref"/>
	<!-- - - - - - - - - - - -->
	<xsl:template match="/">
		<xsl:apply-templates select="xmi:XMI"/>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<xsl:template match="xmi:XMI">
		<xsl:apply-templates select="uml:Model"/>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<xsl:template match="uml:Model">
		<xsl:apply-templates select="packagedElement[ @xmi:type='uml:Package' ][ @xmi:id='ddi4_model' ][ @name='Class Model (Exported from Drupal)' ]" mode="ClassLibrary"/>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<xsl:template match="packagedElement" mode="ClassLibrary">
		<xsl:apply-templates select="packagedElement[ @xmi:type='uml:Package' ]" mode="Package">
			<xsl:sort select="@name"/>
		</xsl:apply-templates>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<xsl:template match="packagedElement" mode="Package">
		<xsl:value-of select="@name"/>
		<xsl:text>&#xA;</xsl:text>
		<xsl:apply-templates select="packagedElement[ @xmi:type='uml:Association' ][ ownedEnd/@aggregation='composite' ]" mode="Composition">
			<xsl:sort select="@name"/>
		</xsl:apply-templates>
	</xsl:template>
	<!-- - - - - - - - - - - -->
	<xsl:template match="packagedElement" mode="Composition">
		<xsl:variable name="SourceSubstring" select="concat( @name, '_', 'source')"/>
		<xsl:variable name="TargetSubstring" select="concat( @name, '_', 'target')"/>
		<xsl:variable name="SourceEnd" select="key( 'Id', memberEnd[ contains( @xmi:idref, $SourceSubstring ) ]/@xmi:idref )"/>
		<xsl:variable name="TargetEnd" select="key( 'Id', memberEnd[ contains( @xmi:idref, $TargetSubstring ) ]/@xmi:idref )"/>
		<xsl:variable name="Source" select="key( 'Id', $SourceEnd/type/@xmi:idref )"/>
		<xsl:variable name="Target" select="key( 'Id', $TargetEnd/type/@xmi:idref )"/>
		<xsl:variable name="SourceLowerValue">
			<xsl:choose>
				<xsl:when test="$SourceEnd/lowerValue/@value">
					<xsl:value-of select="$SourceEnd/lowerValue/@value"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="SourceUpperValue">
			<xsl:choose>
				<xsl:when test="$SourceEnd/upperValue/@value = '-1' ">
					<xsl:text>*</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$SourceEnd/upperValue/@value"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="TargetLowerValue">
			<xsl:choose>
				<xsl:when test="$TargetEnd/lowerValue/@value">
					<xsl:value-of select="$TargetEnd/lowerValue/@value"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="TargetUpperValue">
			<xsl:choose>
				<xsl:when test="$TargetEnd/upperValue/@value='-1' ">
					<xsl:text>*</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$TargetEnd/upperValue/@value"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>  </xsl:text>
		<!-- association name -->
		<xsl:value-of select="@name"/>
		<xsl:text> - Composition </xsl:text>
		<xsl:value-of select="$Source/@name"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$SourceLowerValue"/>
		<xsl:text>..</xsl:text>
		<xsl:value-of select="$SourceUpperValue"/>
		<xsl:text>) (navigavable/arrowhead) -- Composite </xsl:text>
		<xsl:value-of select="$Target/@name"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$TargetLowerValue"/>
		<xsl:text>..</xsl:text>
		<xsl:value-of select="$TargetUpperValue"/>
		<xsl:text>)</xsl:text>
		<xsl:text>&#xA;</xsl:text>
	</xsl:template>
	<!-- - - - - - - - - - - -->
</xsl:stylesheet>
